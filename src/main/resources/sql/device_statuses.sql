insert into device_statuses(id, title, description)
values (1, 'WORKED', 'Работает'),
       (2, 'NOT_WORKED', 'Не работает'),
       (3, 'REPAIR', 'Ремонтируется');
