CREATE TABLE IF NOT EXISTS incidents_details (
	incident_id int8 NOT NULL,
	detail_id int8 NOT NULL,
	count int8 NOT NULL,
	CONSTRAINT incidents_details_pkey PRIMARY KEY (incident_id, detail_id),
	CONSTRAINT fk_details_incidents FOREIGN KEY (detail_id) REFERENCES public.details(id),
	CONSTRAINT fk_incidents_details FOREIGN KEY (incident_id) REFERENCES public.incidents(id)
);