CREATE TABLE IF NOT EXISTS incidents_files (
	id int8 NOT NULL,
	created_by varchar(255) NULL,
	created_when timestamp NULL,
	"path" varchar(255) NULL,
	incident_id int8 NULL,
	file_name varchar(255) NOT NULL,
	CONSTRAINT incidents_files_pkey PRIMARY KEY (id),
	CONSTRAINT fk_incident_files_incident FOREIGN KEY (incident_id) REFERENCES public.incidents(id)
);