CREATE TABLE IF NOT EXISTS device_statuses (
	id int8 NOT NULL,
	created_by varchar(255) NULL,
	created_when timestamp NULL,
	description varchar(255) NOT NULL,
	title varchar(255) NOT NULL,
	CONSTRAINT device_statuses_pkey PRIMARY KEY (id)
);