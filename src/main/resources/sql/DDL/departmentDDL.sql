CREATE TABLE IF NOT EXISTS departments (
	id int8 NOT NULL,
	created_by varchar(255) NULL,
	created_when timestamp NULL,
	description text NULL,
	title varchar(255) NOT NULL,
	CONSTRAINT departments_pkey PRIMARY KEY (id)
);