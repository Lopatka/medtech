CREATE TABLE IF NOT EXISTS details (
	id int8 NOT NULL,
	created_by varchar(255) NULL,
	created_when timestamp NULL,
	amount int4 NULL,
	article_number varchar(255) NULL,
	delivery_date date NULL,
	description text NULL,
	price numeric(19, 2) NULL,
	serial_number varchar(255) NULL,
	title varchar(255) NOT NULL,
	manufacturer_id int8 NOT NULL,
	CONSTRAINT details_amount_check CHECK ((amount >= 0)),
	CONSTRAINT details_pkey PRIMARY KEY (id),
	CONSTRAINT fk_detail_manufacturer FOREIGN KEY (manufacturer_id) REFERENCES public.manufacturers(id)
);