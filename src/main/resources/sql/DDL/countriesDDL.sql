CREATE TABLE IF NOT EXISTS countries (
	id int8 NOT NULL,
	created_by varchar(255) NULL,
	created_when timestamp NULL,
	title varchar(255) NOT NULL,
	CONSTRAINT countries_pkey PRIMARY KEY (id)
);