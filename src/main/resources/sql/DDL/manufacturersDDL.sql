CREATE TABLE IF NOT EXISTS manufacturers (
	id int8 NOT NULL,
	created_by varchar(255) NULL,
	created_when timestamp NULL,
	title varchar(255) NOT NULL,
	country_id int8 NOT NULL,
	CONSTRAINT manufacturers_pkey PRIMARY KEY (id),
	CONSTRAINT fk_manufacturer_country FOREIGN KEY (country_id) REFERENCES public.countries(id)
);