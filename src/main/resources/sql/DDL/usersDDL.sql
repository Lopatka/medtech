CREATE TABLE IF NOT EXISTS users (
	id int8 NOT NULL,
	created_by varchar(255) NULL,
	created_when timestamp NULL,
	backup_email varchar(255) NOT NULL,
	first_name varchar(255) NOT NULL,
	gender varchar(255) NOT NULL,
	last_name varchar(255) NOT NULL,
	login varchar(255) NOT NULL,
	middle_name varchar(255) NOT NULL,
	"password" varchar(255) NOT NULL,
	phone varchar(255) NULL,
	department_id int8 NOT NULL,
	role_id int8 NOT NULL,
	birth_date timestamp NOT NULL,
	CONSTRAINT users_pkey PRIMARY KEY (id),
	CONSTRAINT fk_user_department FOREIGN KEY (department_id) REFERENCES public.departments(id),
	CONSTRAINT fk_user_role FOREIGN KEY (role_id) REFERENCES public.roles(id)
);