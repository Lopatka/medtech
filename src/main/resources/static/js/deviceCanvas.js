async function doRequest(url) {
    let result = await fetch(url).then(res => res.json());
    return result;
}

const resultSumDeviceLabel = document.getElementById("resultSumDevice");
const deviceId = document.getElementById("deviceId").value;
const url = "http://localhost:9090/rest/devices/" + deviceId + "/incidents";

doRequest(url).then((result) => {
    let resultSumDevice = 0;
    let price = [result['incidents'].length];
    let dates = [result['incidents'].length];
    let criticalityColor = [result['incidents'].length];

    for (let i = 0; i < result['incidents'].length; i += 1) {
        price[i] = result['incidents'][i]['fullPrice'];
        resultSumDevice += result['incidents'][i]['fullPrice'];
        dates[i] = result['incidents'][i]['createdWhen'];
        switch (result['incidents'][i]['criticality']) {
          case "LOW":
            criticalityColor[i] = 'rgba(29, 233, 182, 0.6)';
            break;
          case "MEDIUM":
            criticalityColor[i] = 'rgba(255, 152, 0, 0.6)';
            break;
          case "HIGH":
            criticalityColor[i] = 'rgba(216, 27, 66, 0.6)';
            break;
          default:
            criticalityColor[i] = "gray"
        }
    }
    resultSumDeviceLabel.textContent = "Общая сумма: " + resultSumDevice + " руб.";

    let canvas = document.getElementById("chart");
    let ctx = canvas.getContext('2d');
    let chart = new Chart(ctx,{
        type: 'bar',
        data: {
            labels: dates,
            datasets: [{
                label: "Стоимость ремонта",
                data: price,
                borderWidth: 1,
                backgroundColor: criticalityColor
            }],
        },
        options: {
            maintainAspectRatio: true,
            legend: { display: false },
            title: {
                color: '#111',
                fontSize: 18,
                display: true,
                text: 'Заявки'
            }
        }
    })
});