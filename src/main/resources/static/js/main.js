function tdclick(e){
    if (!e) var e = window.event;                // Get the window event
    e.cancelBubble = true;                       // IE Stop propagation
    if (e.stopPropagation) e.stopPropagation();
};

function addDetail() {
    let domParser = new DOMParser();
    let select = document.getElementById("detailsSelect");
    let detail = select.options[select.selectedIndex];
    let incident = document.getElementById("id");
    let repairContent = document.getElementById("repairContent");

    let detailCard = domParser.parseFromString('<div class="p-3 my-2 d-flex justify-content-between detail-card" type="hidden"> </div>', "text/html").getElementsByTagName("div")[0]

    let inputIncident = document.createElement("input");
    let inputDetail = document.createElement("input");
        inputIncident.setAttribute('type', 'hidden');
        inputDetail.setAttribute('type', 'hidden');
        inputIncident.setAttribute('name', 'detailsIds[' + repairContent.children.length + '].incident');
        inputDetail.setAttribute('name', 'detailsIds[' + repairContent.children.length + '].detail');
        inputIncident.setAttribute('value', incident.getAttribute('value'));
        inputDetail.setAttribute('value', detail.getAttribute('value'));
        detailCard.appendChild(inputIncident);
        detailCard.appendChild(inputDetail);

    let detailCardTitle = domParser.parseFromString('<div class="col d-flex flex-column pe-2"> </div>', "text/html").getElementsByTagName("div")[0]
    let spanDetailTitle = document.createElement("span");
        spanDetailTitle.textContent = detail.innerHTML;
        detailCardTitle.appendChild(spanDetailTitle);
        detailCard.appendChild(detailCardTitle);

    let detailCardCount = domParser.parseFromString('<div class="col d-flex flex-column pe-3"> </div>', "text/html").getElementsByTagName("div")[0]
        detailCardCount.setAttribute('data-iter', repairContent.children.length);
    let detailCardCountSpan = document.createElement("span");
        detailCardCountSpan.textContent = "Количество";
        detailCardCount.appendChild(detailCardCountSpan);
    let detailCardCountInput = domParser.parseFromString('<input value=1 type="number" pattern="^[0-9]+$" style="width:120px;" oninput="changeCount(this)">', "text/html").getElementsByTagName("input")[0]
        detailCardCountInput.setAttribute('name', 'detailsIds[' + repairContent.children.length + '].count');
        detailCardCountInput.setAttribute('id','detailsIds[' + repairContent.children.length + '].count');
        detailCardCount.appendChild(detailCardCountInput);
    let detailCardCountPrice = domParser.parseFromString('<div class="small"> </div>', "text/html").getElementsByTagName("div")[0]
        detailCardCountPrice.setAttribute('id','detailPrice[' + repairContent.children.length + ']');
        detailCardCountPrice.textContent = detail.dataset.price + ' руб./шт.';
        detailCardCountPrice.setAttribute('data-price', detail.dataset.price);
        detailCardCount.appendChild(detailCardCountPrice);
    let detailCardCountSum = domParser.parseFromString('<div style="font-weight: bold;"> </div>', "text/html").getElementsByTagName("div")[0]
        detailCardCountSum.setAttribute('name', 'detailSum');
        detailCardCountSum.setAttribute('id','detailSum[' + repairContent.children.length + ']');
        detailCardCountSum.textContent = detail.dataset.price + ' руб./шт.';
        detailCardCountSum.setAttribute('data-price', detail.dataset.price);
        detailCardCount.appendChild(detailCardCountSum);
        detailCard.appendChild(detailCardCount);
   let detailCardButtons = domParser.parseFromString('<div class="col-auto"> </div>', "text/html").getElementsByTagName("div")[0]
        let buttonDelete = domParser.parseFromString('<a class="btn btn-danger" role="button" aria-disabled="true" onclick="removeDetail(this)"> </a>', "text/html").getElementsByTagName("a")[0]
        buttonDelete.textContent = "Удалить";
        detailCardButtons.appendChild(buttonDelete);
        detailCard.appendChild(detailCardButtons);

    if (detail.getAttribute('id') != 'detailNull') {
        repairContent.appendChild(detailCard);
        select.removeChild(detail);
        changeCount(detailCardCountInput);
        changeContentColor()
    }
}

function removeDetail(e) {
    let deletedElem = e.parentNode.parentNode;
    let parent = e.parentNode.parentNode.parentNode;
    parent.removeChild(deletedElem);
    changeContentColor()
    funonload();
}

function changeCount(e) {
    if (e.value <= 0) {
        e.value = 1;
    }
    let parent = e.parentNode
    let price = document.getElementById("detailPrice[" + parent.dataset.iter + "]");
    let sum = document.getElementById("detailSum[" + parent.dataset.iter +"]");
    sum.textContent = (e.value * price.dataset.price) + " руб.";
    sum.dataset.price = e.value * price.dataset.price;

    let detailSum = document.querySelectorAll('[name="detailSum"]');
    let result = 0;
    for (const elem of detailSum) {
        result += Number(elem.dataset.price);
    };
    let resultSum = document.getElementById("resultSum");
    resultSum.textContent = result + " руб.";
    changeContentColor()
}

function changeContentColor() {
    let content = document.getElementById("repair-panel-content");
    let tab = document.getElementById("nav-repair-tab");
    let notify = document.getElementById("notify-message");
    notify.textContent = "Внимание, изменения не сохранены!"
    notify.classList.add("btn", "btn-warning");
    content.style.backgroundColor = 'lightgoldenrodyellow';
    content.style.borderTopColor = 'lightgoldenrodyellow';
    tab.style.backgroundColor = 'lightgoldenrodyellow';
    tab.style.borderBottomColor = 'lightgoldenrodyellow';
}
