package com.sbercourses.medtech;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MedTechApplication {

	public static void main(String[] args) {
		SpringApplication.run(MedTechApplication.class, args);
	}
}
