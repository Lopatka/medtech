package com.sbercourses.medtech.exception;

public class IncidentUpdateStatusException extends Exception {
    public IncidentUpdateStatusException(String message) {
        super(message);
    }
}
