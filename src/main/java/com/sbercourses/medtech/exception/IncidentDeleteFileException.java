package com.sbercourses.medtech.exception;

public class IncidentDeleteFileException extends Exception {
    public IncidentDeleteFileException(String message) {
        super(message);
    }
}
