package com.sbercourses.medtech.exception;

public class IncidentUpdateException extends Exception {
    public IncidentUpdateException(String message){
        super(message);
    }
}
