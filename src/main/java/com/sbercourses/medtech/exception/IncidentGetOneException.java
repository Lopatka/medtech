package com.sbercourses.medtech.exception;

public class IncidentGetOneException extends Exception {
    public IncidentGetOneException(String message) {
        super(message);
    }
}
