package com.sbercourses.medtech.exception;

public class IncidentUpdateDetailsException extends Exception {
    public IncidentUpdateDetailsException(String message){
        super(message);
    }
}
