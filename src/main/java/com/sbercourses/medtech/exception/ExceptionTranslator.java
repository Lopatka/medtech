package com.sbercourses.medtech.exception;

import javassist.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ExceptionTranslator {

    @ExceptionHandler(IncidentGetOneException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ResponseBody
    public ErrorDTO handleIncidentGetOneException(IncidentGetOneException ex) {
        return processFieldErrors(ex, "Получение заявки невозможно", ex.getMessage());
    }

    @ExceptionHandler(IncidentUpdateStatusException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ResponseBody
    public ErrorDTO handleIncidentUpdateStatusException(IncidentUpdateStatusException ex) {
        return processFieldErrors(ex, "Обновление статуса невозможно", ex.getMessage());
    }

    @ExceptionHandler(IncidentCreateFilesException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ResponseBody
    public ErrorDTO handleIncidentCreateFilesException(IncidentCreateFilesException ex) {
        return processFieldErrors(ex, "Добавление файлов невозможно", ex.getMessage());
    }

    @ExceptionHandler(IncidentDeleteFileException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ResponseBody
    public ErrorDTO handleIncidentDeleteFileException(IncidentDeleteFileException ex) {
        return processFieldErrors(ex, "Удаление файлов невозможно", ex.getMessage());
    }

    @ExceptionHandler(IncidentUpdateDetailsException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ResponseBody
    public ErrorDTO handleIncidentUpdateDetailsException(IncidentUpdateDetailsException ex) {
        return processFieldErrors(ex, "Изменение деталей невозможно", ex.getMessage());
    }

    @ExceptionHandler(IncidentUpdateException.class)
    @ResponseStatus(HttpStatus.FORBIDDEN)
    @ResponseBody
    public ErrorDTO handleIncidentUpdateException(IncidentUpdateException ex) {
        return processFieldErrors(ex, "Редактирование заявки невозможно", ex.getMessage());
    }

    private ErrorDTO processFieldErrors(Exception e,
                                        String error,
                                        String description) {
        ErrorDTO errorDTO = new ErrorDTO(error, description);
        errorDTO.add(e.getClass().getName(), "", e.getMessage());
        return errorDTO;
    }
}
