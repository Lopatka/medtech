package com.sbercourses.medtech.service;

import com.sbercourses.medtech.dto.incident.IncidentDTO;
import com.sbercourses.medtech.dto.incident.IncidentDetailsDTO;
import com.sbercourses.medtech.dto.incident.IncidentRestDTO;
import com.sbercourses.medtech.dto.incident.IncidentSearchDTO;
import com.sbercourses.medtech.exception.IncidentGetOneException;
import com.sbercourses.medtech.exception.IncidentUpdateDetailsException;
import com.sbercourses.medtech.exception.IncidentUpdateException;
import com.sbercourses.medtech.exception.IncidentUpdateStatusException;
import com.sbercourses.medtech.model.*;
import com.sbercourses.medtech.model.enums.IncidentStatus;
import com.sbercourses.medtech.model.enums.UserRole;
import com.sbercourses.medtech.repository.*;
import com.sbercourses.medtech.service.userdetails.CustomUserDetails;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Slf4j
@Service
public class IncidentService extends GenericService<Incident, IncidentDTO> {
    private final IncidentRepository incidentRepository;
    private final UserRepository userRepository;
    private final DeviceRepository deviceRepository;
    private final IncidentDetailRepository incidentDetailRepository;
    private final IncidentFileService incidentFileService;
    private final JavaMailSender javaMailSender;
    private static final String INCIDENT_URL = "http://localhost:9090/incidents/";

    public IncidentService(IncidentRepository incidentRepository,
                           UserRepository userRepository,
                           DeviceRepository deviceRepository,
                           IncidentDetailRepository incidentDetailRepository,
                           IncidentFileService incidentFileService, JavaMailSender javaMailSender) {
        this.incidentRepository = incidentRepository;
        this.userRepository = userRepository;
        this.deviceRepository = deviceRepository;
        this.incidentDetailRepository = incidentDetailRepository;
        this.incidentFileService = incidentFileService;
        this.javaMailSender = javaMailSender;
    }

    @Override
    public Incident update(Incident object) {
        return incidentRepository.save(object);
    }

    public Incident updateFromDTOSecure(IncidentDTO object, Long incidentId) throws IncidentUpdateException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Long userId = ((CustomUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal()).getUserId().longValue();
        Incident incident = incidentRepository.findById(incidentId).orElseThrow(
                () -> new NotFoundException("Such incident with id=" + incidentId + " not found"));

        if ((incident.getUser() != null && Objects.equals(incident.getUser().getId(), userId))) {
            return updateFromDTO(object, incidentId);
        } else {
            throw new IncidentUpdateException("Нельзя редактировать, так как нет прав на заявку с id: " + incidentId);
        }
    }

    @Override
    public Incident updateFromDTO(IncidentDTO object, Long incidentId) {
        Long userId = ((CustomUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal()).getUserId().longValue();
        Incident incident;
        incidentRepository.findById(incidentId).orElseThrow(
                () -> new NotFoundException("Such incident with id=" + incidentId + " not found"));
        User user = userRepository.findById(userId).orElseThrow(
                () -> new NotFoundException("Such user with id=" + userId + " not found"));
        Device device = deviceRepository.findById(object.getDevice().getId()).orElseThrow(
                () -> new NotFoundException("Such device with id=" + userId + " not found"));
        incident = Incident.builder()
                .title(object.getTitle())
                .description(object.getDescription())
                .criticality(object.getCriticality())
                .user(user)
                .device(device)
                .status(IncidentStatus.DRAFT)
                .build();
        incident.setId(incidentId);
        incident.setCreatedWhen(LocalDateTime.now());
        return incidentRepository.save(incident);
    }

    @Override
    public Incident createFromDTO(IncidentDTO newDtoObject) {
        User user = userRepository.findById(newDtoObject.getUser().getId()).orElseThrow(
                () -> new NotFoundException("Such user with id=" + newDtoObject.getUser().getId() + " not found"));
        Device device = deviceRepository.findById(newDtoObject.getDevice().getId()).orElseThrow(
                () -> new NotFoundException("Such device with id=" + newDtoObject.getDevice().getId() + " not found"));
        Incident incident = Incident.builder()
                .title(newDtoObject.getTitle())
                .description(newDtoObject.getDescription())
                .criticality(newDtoObject.getCriticality())
                .user(user)
                .device(device)
                .status(IncidentStatus.DRAFT)
                .build();
        return incidentRepository.save(incident);
    }

    public Incident createFromDTO(IncidentRestDTO newDtoObject) {
        User user = userRepository.findById(newDtoObject.getUserId()).orElseThrow(
                () -> new NotFoundException("Such user with id=" + newDtoObject.getUserId() + " not found"));
        Device device = deviceRepository.findById(newDtoObject.getDeviceId()).orElseThrow(
                () -> new NotFoundException("Such device with id=" + newDtoObject.getDeviceId() + " not found"));
        Incident incident = Incident.builder()
                .title(newDtoObject.getTitle())
                .description(newDtoObject.getDescription())
                .criticality(newDtoObject.getCriticality())
                .user(user)
                .device(device)
                .status(IncidentStatus.DRAFT)
                .build();
        return incidentRepository.save(incident);
    }

    @Override
    public Incident createFromEntity(Incident newObject) {
        return incidentRepository.save(newObject);
    }

    @Override
    public void delete(Long objectId) {
        Incident incident = incidentRepository.findById(objectId)
                .orElseThrow(() -> new NotFoundException("Incident with such ID: " + objectId + " not found in the DB"));
        for (Detail detail : incident.getDetails()) {
            incident.getDetails().remove(detail);
        }
        incident.setDetails(new ArrayList<>());
        incidentRepository.deleteById(incident.getId());
    }

    @Override
    public Incident getOne(Long objectId) {
        return incidentRepository.findById(objectId)
                .orElseThrow(() -> new NotFoundException("Incident with such ID: " + objectId + " not found in the DB"));
    }

    public Incident getOneSecure(Long incidentId) throws IncidentGetOneException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals(UserRole.ADMIN.getRoleName()))) {
            return getOne(incidentId);
        }

        Incident incident = incidentRepository.findById(incidentId)
                .orElseThrow(() -> new NotFoundException("Incident with such ID: " + incidentId + " not found in the DB"));
        Long userId = ((CustomUserDetails) authentication.getPrincipal()).getUserId().longValue();

        if ((incident.getUser() != null && Objects.equals(incident.getUser().getId(), userId)) ||
                (incident.getRepairer() == null) ||
                (incident.getRepairer() != null && Objects.equals(incident.getRepairer().getId(), userId))) {
            return incident;
        } else {
            throw new IncidentGetOneException("Нельзя получить заявку, так как нет прав на заявку с id: " + incidentId);
        }
    }

    @Override
    public List<Incident> listAll() {
        return incidentRepository.findAll();
    }

    public List<IncidentRestDTO> listAllDTO() {
        List<IncidentRestDTO> incidentRestDTOS = new ArrayList<>();
        List<Incident> incidents = incidentRepository.findAll();

        for (Incident incident : incidents) {
            incidentRestDTOS.add(new IncidentRestDTO(incident));
        }
        return incidentRestDTOS;
    }

    public List<Incident> listAllByDeviceId(Long id) {
        return incidentRepository.findAllByDeviceId(id);
    }

    public Page<Incident> getAllPaginated(PageRequest pageRequest) {
        Page<Incident> incidents = incidentRepository.findAll(pageRequest);
        return new PageImpl<>(incidents.getContent(), pageRequest, incidents.getTotalElements());
    }

    public Page<Incident> getAllPaginated(PageRequest pageRequest, Authentication auth) {
        Page<Incident> incidents = incidentRepository.findAll(pageRequest);
        List<Incident> incidentList;
        long id;

        if (auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals(UserRole.MEDICAL_WORKER.toString()))) {
            id = ((CustomUserDetails) auth.getPrincipal()).getUserId().longValue();
            incidentList = incidentRepository.findAllByUserId(id);
            incidents = new PageImpl<>(incidentList);
        } else if (auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals(UserRole.ENGINEER.toString()))) {
            id = ((CustomUserDetails) auth.getPrincipal()).getUserId().longValue();
            incidentList = incidentRepository.findAllByRepairerId(id);
            incidents = new PageImpl<>(incidentList);
        }
        return new PageImpl<>(incidents.getContent(), pageRequest, incidents.getTotalElements());
    }

    public Page<Incident> searchIncidents(IncidentSearchDTO incidentSearchDTO, Authentication auth, PageRequest pageRequest) {
        String title = incidentSearchDTO.getTitle().isBlank() ?
                "%" : incidentSearchDTO.getTitle();
        String criticality = incidentSearchDTO.getCriticality() != null ?
                String.valueOf(incidentSearchDTO.getCriticality()) : "%";
        String createdWhen = incidentSearchDTO.getCreatedWhen().isBlank() ?
                "%" : incidentSearchDTO.getCreatedWhen();
        String status = incidentSearchDTO.getStatus() != null ?
                String.valueOf(incidentSearchDTO.getStatus()) : "%";
        Long userId = null;
        Long repairerId = null;

        if (auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals(UserRole.MEDICAL_WORKER.toString()))) {
            userId = ((CustomUserDetails) auth.getPrincipal()).getUserId().longValue();
        } else if (auth.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals(UserRole.ENGINEER.toString()))) {
            repairerId = ((CustomUserDetails) auth.getPrincipal()).getUserId().longValue();
        }
        Page<Incident> incidents = incidentRepository.searchIncidents(
                pageRequest,
                title,
                criticality,
                createdWhen,
                status,
                userId,
                repairerId
        );
        return new PageImpl<>(incidents.getContent(), pageRequest, incidents.getTotalElements());
    }

    public Incident updateStatus(IncidentStatus incidentStatus, Long incidentId) throws IncidentUpdateStatusException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Long userId = ((CustomUserDetails) authentication.getPrincipal()).getUserId().longValue();
        User repairer = userRepository.findById(userId)
                .orElseThrow(() -> new NotFoundException("User-repairer with such ID: " + userId + " not found in the DB"));
        Incident incident = incidentRepository.findById(incidentId)
                .orElseThrow(() -> new NotFoundException("Incident with such ID: " + incidentId + " not found in the DB"));
        if ((incident.getUser() != null && Objects.equals(incident.getUser().getId(), userId)) ||
                (incident.getRepairer() == null) ||
                (incident.getRepairer() != null && Objects.equals(incident.getRepairer().getId(), userId))) {
            IncidentStatus newIncidentStatus = null;
            switch (incidentStatus) {
                case DRAFT:
                    newIncidentStatus = IncidentStatus.NEW;
                    incident.setCreatedWhen(LocalDateTime.now());
                    break;
                case NEW:
                    if (!userId.equals(incident.getUser().getId())) {
                        newIncidentStatus = IncidentStatus.WORKING;
                        incident.setRepairer(repairer);
                        incident.setStartedWorkingWhen(LocalDateTime.now());
                        sendUpdateIncidentStatusEmail(incident, newIncidentStatus, incident.getUser().getBackUpEmail());
                    }
                    break;
                case WORKING:
                    if (userId.equals(incident.getRepairer().getId())) {
                        newIncidentStatus = IncidentStatus.COMPLETED;
                        incident.setCompletedWhen(LocalDateTime.now());
                        sendUpdateIncidentStatusEmail(incident, newIncidentStatus, incident.getUser().getBackUpEmail());
                    }
                    break;
                case COMPLETED:
                    if (userId.equals(incident.getUser().getId())) {
                        newIncidentStatus = IncidentStatus.CLOSED;
                        incident.setClosedWhen(LocalDateTime.now());
                        sendUpdateIncidentStatusEmail(incident, newIncidentStatus, incident.getRepairer().getBackUpEmail());
                    }
                    break;
            }
            incident.setStatus(newIncidentStatus);
        } else {
            throw new IncidentUpdateStatusException("Нельзя изменить статус, так как нет прав на заявку с id: " + incidentId);
        }
        return incident;
    }

    public void sendUpdateIncidentStatusEmail(final Incident incident, final IncidentStatus newStatus, final String backUpEmail) {
        SimpleMailMessage message = new SimpleMailMessage();
        String status = incident.getStatus().getIncidentStatusName();
        message.setTo(backUpEmail);
        message.setSubject("Обновление статуса заявки \"" + incident.getTitle() + "\" на сервисе \"MedTech\"");
        message.setText("Добрый день! Вы получили это письмо, так как статус заявки \"" + incident.getTitle() + "\" был изменен на \"" + newStatus.getIncidentStatusName() + "\". ('" +
                INCIDENT_URL + incident.getId() + "')");
        javaMailSender.send(message);
    }

    public Incident updateDetailsFromDTOSecure(IncidentDetailsDTO incidentDetailsDTO) throws IncidentUpdateDetailsException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Long userId = ((CustomUserDetails) authentication.getPrincipal()).getUserId().longValue();
        Incident incident = incidentRepository.findById(incidentDetailsDTO.getId()).orElseThrow(
                () -> new NotFoundException("Such incident with id=" + incidentDetailsDTO.getId() + " not found"));

        if ((incident.getRepairer() != null && Objects.equals(incident.getRepairer().getId(), userId))) {
            return updateDetailsFromDTO(incident, incidentDetailsDTO);
        } else {
            throw new IncidentUpdateDetailsException("Нельзя изменить детали, так как нет прав на заявку с id: " + incident.getId());
        }
    }

    public Incident updateDetailsFromDTO(Incident incident, IncidentDetailsDTO object) {
        List<IncidentDetail> detailIds = new ArrayList<>();

        if (object.getDetailsIds() != null) {
            for (IncidentDetail incidentDetail : object.getDetailsIds()) {
                if ((incidentDetail.getIncident() != null) &&
                        (incidentDetail.getDetail() != null) &&
                        (incidentDetail.getCount() != null)) {
                    detailIds.add(incidentDetail);
                }
            }
        }

        incident.setDetails(null);
        incident.setRepairDescription(object.getRepairDescription());
        incident = incidentRepository.save(incident);
        incidentDetailRepository.saveAll(detailIds);

        return incident;
    }
}
