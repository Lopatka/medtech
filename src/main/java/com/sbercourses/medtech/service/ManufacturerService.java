package com.sbercourses.medtech.service;

import com.sbercourses.medtech.dto.manufacturer.ManufacturerDTO;

import com.sbercourses.medtech.dto.manufacturer.ManufacturerSearchDTO;
import com.sbercourses.medtech.model.Country;
import com.sbercourses.medtech.model.Manufacturer;
import com.sbercourses.medtech.repository.CountryRepository;
import com.sbercourses.medtech.repository.ManufacturerRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.List;

@Service
public class ManufacturerService extends GenericService<Manufacturer, ManufacturerDTO> {

    private final ManufacturerRepository manufacturerRepository;
    private final CountryRepository countryRepository;

    public ManufacturerService(ManufacturerRepository manufacturerRepository, CountryRepository countryRepository) {
        this.manufacturerRepository = manufacturerRepository;
        this.countryRepository = countryRepository;
    }

    @Override
    public Manufacturer update(Manufacturer object) {
        return manufacturerRepository.save(object);
    }

    @Override
    public Manufacturer updateFromDTO(ManufacturerDTO object, Long objectId) {
        Manufacturer manufacturer = manufacturerRepository.findById(objectId).orElseThrow(
                () -> new NotFoundException("Such manufacturer with id=" + objectId + " not found"));
        Country country = countryRepository.findById(object.getCountry().getId()).orElseThrow(
                () -> new NotFoundException("Such country with id=" + object.getCountry().getId() + " not found"));
        manufacturer.setTitle(object.getTitle());
        manufacturer.setCountry(country);
        return manufacturerRepository.save(manufacturer);
    }

    @Override
    public Manufacturer createFromDTO(ManufacturerDTO newDtoObject) {
        Manufacturer manufacturer = new Manufacturer();
        Country country = countryRepository.findById(newDtoObject.getCountry().getId()).orElseThrow(
                () -> new NotFoundException("Such country with id=" + newDtoObject.getCountry().getId() + " not found"));
        manufacturer.setTitle(newDtoObject.getTitle());
        manufacturer.setCountry(country);
        return manufacturerRepository.save(manufacturer);
    }

    @Override
    public Manufacturer createFromEntity(Manufacturer newObject) {
        return manufacturerRepository.save(newObject);
    }

    @Override
    public void delete(Long objectId) {
        manufacturerRepository.deleteById(objectId);
    }

    @Override
    public Manufacturer getOne(Long objectId) {
        return manufacturerRepository.findById(objectId)
                .orElseThrow(() -> new NotFoundException("Manufacturer with such ID:" + objectId + " not found"));
    }

    @Override
    public List<Manufacturer> listAll() {
        return manufacturerRepository.findAll();
    }

    public Page<Manufacturer> getAllPaginated(PageRequest pageRequest) {
        Page<Manufacturer> manufacturers = manufacturerRepository.findAll(pageRequest);
        return new PageImpl<>(manufacturers.getContent(), pageRequest, manufacturers.getTotalElements());
    }
    public Page<Manufacturer> searchManufacturers(ManufacturerSearchDTO manufacturerSearchDTO, PageRequest pageRequest) {
        String manufacturerTitle = manufacturerSearchDTO.getTitle().isBlank() ? "%" : manufacturerSearchDTO.getTitle();
        String countryId = manufacturerSearchDTO.getCountryId().isBlank() ? "%" : manufacturerSearchDTO.getCountryId();
        Page<Manufacturer> manufacturers = manufacturerRepository.searchManufacturers(
                pageRequest,
                manufacturerTitle,
                countryId
        );
        return new PageImpl<>(manufacturers.getContent(), pageRequest, manufacturers.getTotalElements());
    }
}
