package com.sbercourses.medtech.service;

import com.sbercourses.medtech.dto.RoleDTO;
import com.sbercourses.medtech.model.Role;
import com.sbercourses.medtech.repository.RoleRepository;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.List;

@Service
public class RoleService extends GenericService<Role, RoleDTO>{

    private final RoleRepository roleRepository;

    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public Role update(Role object) {
        return roleRepository.save(object);
    }

    @Override
    public Role updateFromDTO(RoleDTO object, Long objectId) {
        Role role = roleRepository.findById(objectId).orElseThrow(
                () -> new NotFoundException("Such role with id=" + objectId + " now found"));
        role.setTitle(object.getTitle());
        role.setDescription(object.getDescription());
        return roleRepository.save(role);
    }

    @Override
    public Role createFromDTO(RoleDTO newDtoObject) {
        Role role = new Role();
        role.setTitle(newDtoObject.getTitle());
        role.setDescription(newDtoObject.getDescription());
        return roleRepository.save(role);
    }

    @Override
    public Role createFromEntity(Role newObject) {
        return roleRepository.save(newObject);
    }

    @Override
    public void delete(Long objectId) {
        roleRepository.deleteById(objectId);
    }

    @Override
    public Role getOne(Long objectId) {
        return roleRepository.findById(objectId)
                .orElseThrow(() -> new NotFoundException("Role with such ID: " + objectId + " not found in the DB"));
    }

    @Override
    public List<Role> listAll() {
        return roleRepository.findAll();
    }
}
