package com.sbercourses.medtech.service;

import com.sbercourses.medtech.exception.IncidentCreateFilesException;
import com.sbercourses.medtech.exception.IncidentDeleteFileException;
import com.sbercourses.medtech.exception.IncidentGetOneException;
import com.sbercourses.medtech.model.Incident;
import com.sbercourses.medtech.model.IncidentFile;
import com.sbercourses.medtech.model.enums.UserRole;
import com.sbercourses.medtech.repository.IncidentFileRepository;
import com.sbercourses.medtech.repository.IncidentRepository;
import com.sbercourses.medtech.service.userdetails.CustomUserDetails;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.webjars.NotFoundException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@Slf4j
@Service
public class IncidentFileService {
    private final IncidentFileRepository incidentFileRepository;
    private final IncidentRepository incidentRepository;
    private static final String UPLOAD_DIRECTORY = "files\\incidents";

    public IncidentFileService(IncidentFileRepository incidentFileRepository, IncidentRepository incidentRepository) {
        this.incidentFileRepository = incidentFileRepository;
        this.incidentRepository = incidentRepository;
    }

    public IncidentFile getOneSecure(Long incidentId, Long fileId) throws IncidentGetOneException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals(UserRole.ADMIN.getRoleName()))) {
            return getOne(fileId);
        }

        Incident incident = incidentRepository.findById(incidentId)
                .orElseThrow(() -> new NotFoundException("Incident with such ID: " + incidentId + " not found in the DB"));
        Long userId = ((CustomUserDetails) authentication.getPrincipal()).getUserId().longValue();

        if ((incident.getUser() != null && Objects.equals(incident.getUser().getId(), userId)) ||
                (incident.getRepairer() != null && Objects.equals(incident.getRepairer().getId(), userId))) {
            return getOne(fileId);
        } else {
            throw new IncidentGetOneException("Нельзя получить файл, так как нет прав на заявку с id: " + incidentId);
        }
    }

    public IncidentFile getOne(Long id) {
        return incidentFileRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Incident file with such ID: " + id + " not found in the DB"));
    }

    public List<IncidentFile> listAllById(Long id) {
        return incidentFileRepository.findAllByIncidentId(id);
    }

    public void createFilesSecure(MultipartFile[] files, Long incidentId) throws IncidentCreateFilesException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals(UserRole.ADMIN.getRoleName()))) {
            createFiles(files, incidentId);
            return;
        }
        Incident incident = incidentRepository.findById(incidentId)
                .orElseThrow(() -> new NotFoundException("Incident with such ID: " + incidentId + " not found in the DB"));
        Long userId = ((CustomUserDetails) authentication.getPrincipal()).getUserId().longValue();

        if ((incident.getUser() != null && Objects.equals(incident.getUser().getId(), userId)) ||
                (incident.getRepairer() != null && Objects.equals(incident.getRepairer().getId(), userId))) {
            createFiles(files, incidentId);
        } else {
            throw new IncidentCreateFilesException("Нельзя добавить файлы, так как нет прав на заявку с id: " + incidentId);
        }
    }

    public void createFiles(MultipartFile[] files, Long incidentId) {
        for (MultipartFile file : files) {
            if ((file != null) && (file.getSize() > 0)) {
                createFileSecure(file, incidentId);
            }
        }
    }

    public IncidentFile createFileSecure(MultipartFile file, Long id) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String createdBy;

        if (authentication.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals(UserRole.ADMIN.getRoleName()))) {
            createdBy = "ADMIN";
        } else {
            createdBy = ((CustomUserDetails) SecurityContextHolder.getContext()
                    .getAuthentication().getPrincipal()).getUsername();
        }

        Incident incident = incidentRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Incident with such ID: " + id + " not found in the DB"));
        String fileName = createFile(file, id);
        IncidentFile incidentFile = incidentFileRepository.findByFileNameAndIncidentId(fileName, id);

        if (incidentFile == null) {
            String path = UPLOAD_DIRECTORY + "\\" + id + "\\" + fileName;
            incidentFile = new IncidentFile();
            incidentFile.setFileName(fileName);
            incidentFile.setPath(path);
            incidentFile.setIncident(incident);
            incidentFile.setCreatedWhen(LocalDateTime.now());
            incidentFile.setCreatedBy(createdBy);
            return incidentFileRepository.save(incidentFile);
        }
        return incidentFile;
    }

    public String createFile(MultipartFile file, Long id) {
        String fileName = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));

        try {
            Path path = Paths.get(UPLOAD_DIRECTORY + "\\" + id + "\\" + fileName).toAbsolutePath().normalize();
            if (!path.toFile().exists()) {
                Files.createDirectories(path);
            }
            Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            log.error("IncidentFileService#createFile(): {}", e.getMessage());
        }
        return fileName;
    }

    public void deleteFileSecure(Long fileId, Long incidentId) throws IncidentDeleteFileException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        IncidentFile incidentFile = incidentFileRepository.findById(fileId)
                .orElseThrow(() -> new NotFoundException("Incident file with such ID: " + fileId + " not found in the DB"));

        if (authentication.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals(UserRole.ADMIN.getRoleName()))) {
            deleteFile(incidentFile.getPath(), fileId);
            return;
        }

        Incident incident = incidentRepository.findById(incidentId)
                .orElseThrow(() -> new NotFoundException("Incident with such ID: " + incidentId + " not found in the DB"));
        Long userId = ((CustomUserDetails) authentication.getPrincipal()).getUserId().longValue();

        if ((incident.getUser() != null && Objects.equals(incident.getUser().getId(), userId)) ||
                (incident.getRepairer() != null && Objects.equals(incident.getRepairer().getId(), userId))) {
            deleteFile(incidentFile.getPath(), fileId);
        } else {
            throw new IncidentDeleteFileException("Нельзя удалить файл, так как нет прав на заявку с id: " + incidentId);
        }
    }

    public void deleteFile(String path, Long fileId) {
        try {
            Files.delete(Paths.get(path));
            incidentFileRepository.deleteById(fileId);
        } catch (IOException e) {
            log.error("IncidentFileService#deleteFile(): {}", e.getMessage());
        }
    }

    public void deleteAllFilesSecure(Long incidentId) throws IncidentDeleteFileException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getAuthorities().stream().anyMatch(a -> a.getAuthority().equals(UserRole.ADMIN.getRoleName()))) {
            deleteAllFiles(incidentId);
            return;
        }

        Incident incident = incidentRepository.findById(incidentId)
                .orElseThrow(() -> new NotFoundException("Incident with such ID: " + incidentId + " not found in the DB"));
        Long userId = ((CustomUserDetails) authentication.getPrincipal()).getUserId().longValue();

        if ((incident.getUser() != null && Objects.equals(incident.getUser().getId(), userId)) ||
                (incident.getRepairer() != null && Objects.equals(incident.getRepairer().getId(), userId))) {
            deleteAllFiles(incidentId);
        } else {
            throw new IncidentDeleteFileException("Нельзя удалить файл, так как нет прав на заявку с id: " + incidentId);
        }
    }

    public void deleteAllFiles(Long incidentId) {
        List<IncidentFile> incidentFiles = incidentFileRepository.findAllByIncidentId(incidentId);
        String path = UPLOAD_DIRECTORY + "\\" + incidentId;
        for (IncidentFile incidentFile : incidentFiles) {
            deleteFile(incidentFile.getPath(), incidentFile.getId());
        }
        try {
            Files.delete(Paths.get(path));
        } catch (IOException e) {
            log.error("IncidentFileService#deleteFile(): {}", e.getMessage());
        }
    }
}
