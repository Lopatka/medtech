package com.sbercourses.medtech.service;

import com.sbercourses.medtech.dto.LoginDTO;
import com.sbercourses.medtech.dto.user.UserDTO;
import com.sbercourses.medtech.dto.user.UserSearchDTO;
import com.sbercourses.medtech.model.Department;
import com.sbercourses.medtech.model.Role;
import com.sbercourses.medtech.model.User;
import com.sbercourses.medtech.repository.DepartmentRepository;
import com.sbercourses.medtech.repository.RoleRepository;
import com.sbercourses.medtech.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@Service
public class UserService extends GenericService<User, UserDTO> {

    private static final String CHANGE_PASSWORD_URL = "http://localhost:9090/users/change-password/";
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final DepartmentRepository departmentRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private final JavaMailSender javaMailSender;

    public UserService(UserRepository userRepository, RoleRepository roleRepository, DepartmentRepository departmentRepository, JavaMailSender javaMailSender) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.departmentRepository = departmentRepository;
        this.javaMailSender = javaMailSender;
    }

    @Autowired
    public void setBCryptPasswordEncoder(BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public boolean checkPassword(LoginDTO loginDTO) {
        return bCryptPasswordEncoder.matches(loginDTO.getPassword(), getByUserName(loginDTO.getUsername()).getPassword());
    }

    public User getByUserName(final String name) {
        return userRepository.findUserByLogin(name);
    }

    @Override
    public User update(User object) {
        return userRepository.save(object);
    }

    @Override
    public User updateFromDTO(UserDTO object, Long objectId) {
        User user = userRepository.findUserByLogin(object.getLogin());

        if (user != null) {
            if (!user.getId().equals(objectId)){
                return null;
            }
        }
        if (object.getLogin().equals("admin")){
            return null;
        }

        user = getOne(objectId);
        Role role = roleRepository.findById(object.getRole().getId())
                .orElseThrow(() -> new NotFoundException("Role with such ID: " + object.getRole().getId() + " not found."));
        Department department = departmentRepository.findById(object.getDepartment().getId())
                .orElseThrow(() -> new NotFoundException("Department with such ID: " + object.getDepartment().getId() + " not found."));
        user.setRole(role);
        user.setDepartment(department);
        user.setLastName(object.getLastName());
        user.setFirstName(object.getFirstName());
        user.setMiddleName(object.getMiddleName());
        user.setGender(object.getGender());
        user.setLogin(object.getLogin());
        user.setPhone(object.getPhone());
        user.setBackUpEmail(object.getBackUpEmail());
        user.setBirthDate(LocalDate.parse(object.getBirthDate()));
        return userRepository.save(user);
    }

    @Override
    public User createFromDTO(UserDTO newDtoObject) {
        User user = userRepository.findUserByLogin(newDtoObject.getLogin());

        if (user != null) {
            return null;
        }
        if (newDtoObject.getLogin().equals("admin")){
            return null;
        }

        user = new User();
        Role role = roleRepository.findById(newDtoObject.getRole().getId())
                .orElseThrow(() -> new NotFoundException("Role with such ID: " + newDtoObject.getRole().getId() + " not found."));
        Department department = departmentRepository.findById(newDtoObject.getDepartment().getId())
                .orElseThrow(() -> new NotFoundException("Department with such ID: " + newDtoObject.getDepartment().getId() + " not found."));
        user.setCreatedWhen(LocalDateTime.now());
        user.setCreatedBy("REGISTRATION FORM");
        user.setRole(role);
        user.setDepartment(department);
        user.setLastName(newDtoObject.getLastName());
        user.setFirstName(newDtoObject.getFirstName());
        user.setMiddleName(newDtoObject.getMiddleName());
        user.setGender(newDtoObject.getGender());
        user.setLogin(newDtoObject.getLogin());
        user.setPassword(bCryptPasswordEncoder.encode(newDtoObject.getPassword()));
        user.setPhone(newDtoObject.getPhone());
        user.setBackUpEmail(newDtoObject.getBackUpEmail());
        user.setBirthDate(LocalDate.parse(newDtoObject.getBirthDate()));
        return userRepository.save(user);
    }

    @Override
    public User createFromEntity(User newObject) {
        newObject.setPassword(bCryptPasswordEncoder.encode(newObject.getPassword()));
        return userRepository.save(newObject);
    }

    @Override
    public void delete(Long objectId) {
        userRepository.deleteById(objectId);
    }

    @Override
    public User getOne(Long objectId) {
        return userRepository.findById(objectId)
                .orElseThrow(() -> new NotFoundException("User with such ID: " + objectId + " not found in the DB"));
    }

    @Override
    public List<User> listAll() {
        return userRepository.findAll();
    }

    public UserDTO getUserByEmail(final String email) {
        return new UserDTO(userRepository.findByBackUpEmail(email));
    }

    public void sendChangePasswordEmail(final String backUpEmail,
                                        final Long userId) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(backUpEmail);
        message.setSubject("Восстановление пароля на сервисе \"MedTech\"");
        message.setText("Добрый день! Вы получили это письмо, так как с вашего аккаунта была отправлена заявка на восстановление пароля. " +
                "Для восстановления пароля, перейдите по ссылке: '" + CHANGE_PASSWORD_URL + userId + "'");
        javaMailSender.send(message);
    }

    public void changePassword(Long userId, String password) {
        User user = userRepository.findById(userId).orElseThrow(() -> new NotFoundException("User with such ID: " + userId + " not found."));
        user.setPassword(bCryptPasswordEncoder.encode(password));
        userRepository.save(user);
    }

    public Page<User> getAllPaginated(Pageable pageRequest) {
        Page<User> users = userRepository.findAll(pageRequest);
        return new PageImpl<>(users.getContent(), pageRequest, users.getTotalElements());
    }

    public Page<User> searchUsers(UserSearchDTO userSearchDTO, Pageable pageable) {
        String lastName = userSearchDTO.getLastName().isBlank() ? "%" : userSearchDTO.getLastName();
        String middleName = userSearchDTO.getMiddleName().isBlank() ? "%" : userSearchDTO.getMiddleName();
        String firstName = userSearchDTO.getFirstName().isBlank() ? "%" : userSearchDTO.getFirstName();
        String departmentId = userSearchDTO.getDepartmentId().isBlank() ? "%" : userSearchDTO.getDepartmentId();
        String roleId = userSearchDTO.getRoleId().isBlank() ? "%" : userSearchDTO.getRoleId();
        Page<User> users = userRepository.searchUsers(
                pageable,
                lastName,
                firstName,
                middleName,
                departmentId,
                roleId
        );
        return new PageImpl<>(users.getContent(), pageable, users.getTotalElements());
    }
}
