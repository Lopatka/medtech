package com.sbercourses.medtech.service;


import com.sbercourses.medtech.dto.device.DeviceDTO;
import com.sbercourses.medtech.dto.device.DeviceIncidentDTO;
import com.sbercourses.medtech.dto.device.DeviceSearchDTO;
import com.sbercourses.medtech.dto.device.DeviceUserSearchDTO;
import com.sbercourses.medtech.model.*;
import com.sbercourses.medtech.repository.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class DeviceService extends GenericService<Device, DeviceDTO> {

    private final DeviceRepository deviceRepository;
    private final DeviceStatusRepository deviceStatusRepository;
    private final UserRepository userRepository;
    private final IncidentRepository incidentRepository;
    private final IncidentFileService incidentFileService;
    private final DepartmentRepository departmentRepository;
    private final ManufacturerRepository manufacturerRepository;
    private final DetailService detailService;

    public DeviceService(DeviceRepository deviceRepository,
                         DeviceStatusRepository deviceStatusRepository,
                         UserRepository userRepository,
                         IncidentRepository incidentRepository,
                         IncidentFileService incidentFileService,
                         DepartmentRepository departmentRepository,
                         ManufacturerRepository manufacturerRepository,
                         DetailService detailService) {
        this.deviceRepository = deviceRepository;
        this.deviceStatusRepository = deviceStatusRepository;
        this.userRepository = userRepository;
        this.incidentRepository = incidentRepository;
        this.incidentFileService = incidentFileService;
        this.departmentRepository = departmentRepository;
        this.manufacturerRepository = manufacturerRepository;
        this.detailService = detailService;
    }

    @Override
    public Device update(Device object) {
        return deviceRepository.save(object);
    }

    @Override
    public Device updateFromDTO(DeviceDTO object, Long objectId) {
        Device device;
        deviceRepository.findById(objectId).orElseThrow(
                () -> new NotFoundException("Such device with id=" + objectId + " not found"));
        User user = userRepository.findById(object.getUser().getId()).orElseThrow(
                () -> new NotFoundException("Such user with id=" + object.getUser().getId() + " not found"));
        Department department = departmentRepository.findById(object.getDepartment().getId()).orElseThrow(
                () -> new NotFoundException("Such department with id=" + object.getDepartment().getId() + " not found"));
        Manufacturer manufacturer = manufacturerRepository.findById(object.getManufacturer().getId()).orElseThrow(
                () -> new NotFoundException("Such manufacturer with id=" + object.getManufacturer().getId() + " not found"));
        DeviceStatus deviceStatus = deviceStatusRepository.findById(object.getStatus().getId()).orElseThrow(
                () -> new NotFoundException("Such device status with id=" + object.getStatus() + " not found"));

        device = Device.builder()
                .title(object.getTitle())
                .description(object.getDescription())
                .user(user)
                .department(department)
                .manufacturer(manufacturer)
                .status(deviceStatus)
                .model(object.getModel())
                .serialNumber(object.getSerialNumber())
                .inventoryNumber(object.getInventoryNumber())
                .price(object.getPrice())
                .build();
        device.setId(objectId);
        if (object.getDatePurchase().isEmpty()) {
            device.setDatePurchase(null);
        } else {
            device.setDatePurchase(LocalDate.parse(object.getDatePurchase()));
        }
        return deviceRepository.save(device);
    }

    @Override
    public Device createFromDTO(DeviceDTO newDtoObject) {
        User user = userRepository.findById(newDtoObject.getUser().getId()).orElseThrow(
                () -> new NotFoundException("Such user with id=" + newDtoObject.getUser().getId() + " not found"));
        Department department = departmentRepository.findById(newDtoObject.getDepartment().getId()).orElseThrow(
                () -> new NotFoundException("Such department with id=" + newDtoObject.getDepartment().getId() + " not found"));
        Manufacturer manufacturer = null;
        if (newDtoObject.getManufacturer().getId() != null) {
            manufacturer = manufacturerRepository.findById(newDtoObject.getManufacturer().getId()).orElseThrow(
                    () -> new NotFoundException("Such manufacturer with id=" + newDtoObject.getManufacturer().getId() + " not found"));
        }
        DeviceStatus deviceStatus = deviceStatusRepository.findById(newDtoObject.getStatus().getId()).orElseThrow(
                () -> new NotFoundException("Such device status with id=" + newDtoObject.getStatus().getId() + " not found"));
        Device device = Device.builder()
                .title(newDtoObject.getTitle())
                .description(newDtoObject.getDescription())
                .user(user)
                .department(department)
                .manufacturer(manufacturer)
                .status(deviceStatus)
                .model(newDtoObject.getModel())
                .serialNumber(newDtoObject.getSerialNumber())
                .inventoryNumber(newDtoObject.getInventoryNumber())
                .price(newDtoObject.getPrice())
                .build();
        if (newDtoObject.getDatePurchase().isEmpty()) {
            device.setDatePurchase(null);
        } else {
            device.setDatePurchase(LocalDate.parse(newDtoObject.getDatePurchase()));
        }
        return deviceRepository.save(device);
    }

    @Override
    public Device createFromEntity(Device newObject) {
        return deviceRepository.save(newObject);
    }

    @Override
    public void delete(Long objectId) {
        List<Incident> incidents = incidentRepository.findAllByDeviceId(objectId);

        for (Incident incident : incidents) {
            incidentFileService.deleteAllFiles(incident.getId());
        }
        deviceRepository.deleteById(objectId);
    }

    @Override
    public Device getOne(Long objectId) {
        return deviceRepository.findById(objectId)
                .orElseThrow(() -> new NotFoundException("Device with such ID: " + objectId + " not found in the DB"));
    }

    @Override
    public List<Device> listAll() {
        return deviceRepository.findAll();
    }

    public Page<Device> getAllPaginated(PageRequest pageRequest) {
        Page<Device> devices = deviceRepository.findAll(pageRequest);
        return new PageImpl<>(devices.getContent(), pageRequest, devices.getTotalElements());
    }

    public Page<Device> getAllPaginated(PageRequest pageRequest, Set<Device> devicesSet) {
        Page<Device> devices = new PageImpl<>(new ArrayList<>(devicesSet));
        return new PageImpl<>(devices.getContent(), pageRequest, devices.getTotalElements());
    }

    public Page<Device> searchDevices(DeviceSearchDTO deviceSearchDTO, PageRequest pageRequest) {
        String title = deviceSearchDTO.getTitle().isBlank() ? "%" : deviceSearchDTO.getTitle();
        String departmentId = deviceSearchDTO.getDepartmentId().isBlank() ? "%" : deviceSearchDTO.getDepartmentId();
        String userId = deviceSearchDTO.getUserId().isBlank() ? "%" : deviceSearchDTO.getUserId();
        String statusId = deviceSearchDTO.getStatusId().isBlank() ? "%" : deviceSearchDTO.getStatusId();
        Page<Device> devices = deviceRepository.searchDevices(
                pageRequest,
                title,
                departmentId,
                userId,
                statusId
        );
        return new PageImpl<>(devices.getContent(), pageRequest, devices.getTotalElements());
    }

    public Page<Device> searchDevices(DeviceUserSearchDTO deviceUserSearchDTO, PageRequest pageRequest) {
        String userId = deviceUserSearchDTO.getUserId().isBlank() ? "%" : deviceUserSearchDTO.getUserId();
        String title = deviceUserSearchDTO.getTitle().isBlank() ? "%" : deviceUserSearchDTO.getTitle();
        String serialNumber = deviceUserSearchDTO.getSerialNumber().isBlank() ? "%" : deviceUserSearchDTO.getSerialNumber();
        String inventoryNumber = deviceUserSearchDTO.getInventoryNumber().isBlank() ? "%" : deviceUserSearchDTO.getInventoryNumber();
        String statusId = deviceUserSearchDTO.getStatusId().isBlank() ? "%" : deviceUserSearchDTO.getStatusId();
        Page<Device> devices = deviceRepository.searchDevices(
                pageRequest,
                userId,
                title,
                serialNumber,
                inventoryNumber,
                statusId
        );
        return new PageImpl<>(devices.getContent(), pageRequest, devices.getTotalElements());
    }

    public DeviceIncidentDTO getDeviceIncidents(Long objectId) {
        Device device = deviceRepository.findById(objectId)
                .orElseThrow(() -> new NotFoundException("Device with such ID: " + objectId + " not found in the DB"));
        List<IncidentDetail> incidentDetails = new ArrayList<>();
        for (Incident incident : device.getIncidents()) {
            incidentDetails.addAll(detailService.findIncidentDetails(incident.getId()));
        }
        return new DeviceIncidentDTO(incidentDetails);
    }
}
