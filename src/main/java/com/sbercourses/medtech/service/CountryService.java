package com.sbercourses.medtech.service;


import com.sbercourses.medtech.dto.CountryDTO;
import com.sbercourses.medtech.model.Country;
import com.sbercourses.medtech.repository.CountryRepository;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.List;

@Service
public class CountryService extends GenericService<Country, CountryDTO> {

    private final CountryRepository countryRepository;

    public CountryService(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    @Override
    public Country update(Country object) {
        return countryRepository.save(object);
    }

    @Override
    public Country updateFromDTO(CountryDTO object, Long objectId) {
        Country country = countryRepository.findById(objectId).orElseThrow(
                () -> new NotFoundException("Such country with id=" + objectId + " not found"));
        country.setTitle(object.getTitle());
        return countryRepository.save(country);
    }

    @Override
    public Country createFromDTO(CountryDTO newDtoObject) {
        Country country = new Country();
        country.setTitle(newDtoObject.getTitle());
        return countryRepository.save(country);
    }

    @Override
    public Country createFromEntity(Country newObject) {
        return countryRepository.save(newObject);
    }

    @Override
    public void delete(Long objectId) {
        countryRepository.deleteById(objectId);
    }

    @Override
    public Country getOne(Long objectId) {
        return countryRepository.findById(objectId)
                .orElseThrow(() -> new NotFoundException("Country with such ID: " + objectId + " not found in the DB"));
    }

    @Override
    public List<Country> listAll() {
        return countryRepository.findAll();
    }
}
