package com.sbercourses.medtech.service;

import com.sbercourses.medtech.dto.detail.DetailDTO;
import com.sbercourses.medtech.dto.detail.DetailSearchDTO;
import com.sbercourses.medtech.model.Detail;
import com.sbercourses.medtech.model.IncidentDetail;
import com.sbercourses.medtech.model.Manufacturer;
import com.sbercourses.medtech.repository.DetailRepository;
import com.sbercourses.medtech.repository.IncidentDetailRepository;
import com.sbercourses.medtech.repository.ManufacturerRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.time.LocalDate;
import java.util.List;

@Service
public class DetailService extends GenericService<Detail, DetailDTO> {
    private final DetailRepository detailRepository;
    private final IncidentDetailRepository incidentDetailRepository;
    private final ManufacturerRepository manufacturerRepository;

    public DetailService(DetailRepository detailRepository, IncidentDetailRepository incidentDetailRepository, ManufacturerRepository manufacturerRepository) {
        this.detailRepository = detailRepository;
        this.incidentDetailRepository = incidentDetailRepository;
        this.manufacturerRepository = manufacturerRepository;
    }

    @Override
    public Detail update(Detail object) {
        return detailRepository.save(object);
    }

    @Override
    public Detail updateFromDTO(DetailDTO object, Long objectId) {
        Detail detail;
        detailRepository.findById(objectId).orElseThrow(
                () -> new NotFoundException("Such detail with id=" + objectId + " not found"));
        Manufacturer manufacturer = manufacturerRepository.findById(object.getManufacturer().getId()).orElseThrow(
                () -> new NotFoundException("Such manufacturer with id=" + object.getManufacturer().getId() + " not found"));
        detail = Detail.builder()
                .title(object.getTitle())
                .description(object.getDescription())
                .manufacturer(manufacturer)
                .serialNumber(object.getSerialNumber())
                .articleNumber(object.getArticleNumber())
                .price(object.getPrice())
                .amount(object.getAmount())
                .build();
        detail.setId(objectId);
        if (object.getDeliveryDate().isEmpty()){
            detail.setDeliveryDate(null);
        } else {
            detail.setDeliveryDate(LocalDate.parse(object.getDeliveryDate()));
        }
        return detailRepository.save(detail);
    }

    @Override
    public Detail createFromDTO(DetailDTO newDtoObject) {
        Manufacturer manufacturer = manufacturerRepository.findById(newDtoObject.getManufacturer().getId()).orElseThrow(
                () -> new NotFoundException("Such manufacturer with id=" + newDtoObject.getManufacturer().getId() + " not found"));
        Detail detail = Detail.builder()
                .title(newDtoObject.getTitle())
                .description(newDtoObject.getDescription())
                .manufacturer(manufacturer)
                .serialNumber(newDtoObject.getSerialNumber())
                .articleNumber(newDtoObject.getArticleNumber())
                .price(newDtoObject.getPrice())
                .amount(newDtoObject.getAmount())
                .build();
        if (newDtoObject.getDeliveryDate().isEmpty()){
            detail.setDeliveryDate(null);
        } else {
            detail.setDeliveryDate(LocalDate.parse(newDtoObject.getDeliveryDate()));
        }
        return detailRepository.save(detail);
    }

    @Override
    public Detail createFromEntity(Detail newObject) {
        return detailRepository.save(newObject);
    }

    @Override
    public void delete(Long objectId) {
        detailRepository.deleteById(objectId);
    }

    @Override
    public Detail getOne(Long objectId) {
        return detailRepository.findById(objectId)
                .orElseThrow(() -> new NotFoundException("Such detail with id=" + objectId + " not found"));
    }
    public List<IncidentDetail> findIncidentDetails(Long incidentId){
        return incidentDetailRepository.findDetailsByIncidentId(incidentId);
    }

    @Override
    public List<Detail> listAll() {
        return detailRepository.findAll();
    }

    public Page<Detail> getAllPaginated(PageRequest pageRequest) {
        Page<Detail> details = detailRepository.findAll(pageRequest);
        return new PageImpl<>(details.getContent(), pageRequest, details.getTotalElements());
    }

    public Page<Detail> searchDetails(DetailSearchDTO detailSearchDTO, PageRequest pageRequest) {
        String title = detailSearchDTO.getTitle().isBlank() ? "%" : detailSearchDTO.getTitle();
        String manufacturerId = detailSearchDTO.getManufacturerId().isBlank() ? "%" : detailSearchDTO.getManufacturerId();
        String articleNumber = detailSearchDTO.getArticleNumber().isBlank() ? "%" : detailSearchDTO.getArticleNumber();
        String serialNumber = detailSearchDTO.getSerialNumber().isBlank() ? "%" : detailSearchDTO.getSerialNumber();
        Page<Detail> details = detailRepository.searchDetails(
                pageRequest,
                title,
                manufacturerId,
                articleNumber,
                serialNumber
        );
        return new PageImpl<>(details.getContent(), pageRequest, details.getTotalElements());
    }
}
