package com.sbercourses.medtech.service;

import com.sbercourses.medtech.dto.device.DeviceStatusDTO;
import com.sbercourses.medtech.model.DeviceStatus;
import com.sbercourses.medtech.repository.DeviceStatusRepository;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.List;

@Service
public class DeviceStatusService extends GenericService<DeviceStatus, DeviceStatusDTO> {

    private final DeviceStatusRepository deviceStatusRepository;

    public DeviceStatusService(DeviceStatusRepository deviceStatusRepository) {
        this.deviceStatusRepository = deviceStatusRepository;
    }

    @Override
    public DeviceStatus update(DeviceStatus object) {
        return deviceStatusRepository.save(object);
    }

    @Override
    public DeviceStatus updateFromDTO(DeviceStatusDTO object, Long objectId) {
        DeviceStatus deviceStatus = deviceStatusRepository.findById(objectId).orElseThrow(
                () -> new NotFoundException("Such device status with id=" + objectId + " not found"));
        deviceStatus.setTitle(object.getTitle());
        deviceStatus.setDescription(object.getDescription());
        return deviceStatusRepository.save(deviceStatus);
    }

    @Override
    public DeviceStatus createFromDTO(DeviceStatusDTO newDtoObject) {
        DeviceStatus deviceStatus = new DeviceStatus();
        deviceStatus.setTitle(newDtoObject.getTitle());
        deviceStatus.setDescription(newDtoObject.getDescription());
        return deviceStatusRepository.save(deviceStatus);
    }

    @Override
    public DeviceStatus createFromEntity(DeviceStatus newObject) {
        return deviceStatusRepository.save(newObject);
    }

    @Override
    public void delete(Long objectId) {
        deviceStatusRepository.deleteById(objectId);
    }

    @Override
    public DeviceStatus getOne(Long objectId) {
        return deviceStatusRepository.findById(objectId)
                .orElseThrow(() -> new NotFoundException("Device status with such ID: " + objectId + " not found in the DB"));
    }

    @Override
    public List<DeviceStatus> listAll() {
        return deviceStatusRepository.findAll();
    }
}
