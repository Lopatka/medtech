package com.sbercourses.medtech.service;


import com.sbercourses.medtech.dto.DepartmentDTO;
import com.sbercourses.medtech.model.Department;


import com.sbercourses.medtech.repository.DepartmentRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

import java.util.List;

@Service
public class DepartmentService extends GenericService<Department, DepartmentDTO>{

    private final DepartmentRepository departmentRepository;

    public DepartmentService(DepartmentRepository departmentRepository) {
        this.departmentRepository = departmentRepository;
    }

    @Override
    public Department update(Department object) {
        return departmentRepository.save(object);
    }

    @Override
    public Department updateFromDTO(DepartmentDTO object, Long objectId) {
        Department department = departmentRepository.findById(objectId).orElseThrow(
                () -> new NotFoundException("Such department with id=" + objectId + " not found"));
        department.setTitle(object.getTitle());
        department.setDescription(object.getDescription());
        return departmentRepository.save(department);
    }

    @Override
    public Department createFromDTO(DepartmentDTO newDtoObject) {
        Department department = new Department();
        department.setTitle(newDtoObject.getTitle());
        department.setDescription(newDtoObject.getDescription());
        return departmentRepository.save(department);
    }

    @Override
    public Department createFromEntity(Department newObject) {
        return departmentRepository.save(newObject);
    }

    @Override
    public void delete(Long objectId) {
        departmentRepository.deleteById(objectId);
    }

    @Override
    public Department getOne(Long objectId) {
        return departmentRepository.findById(objectId)
                .orElseThrow(() -> new NotFoundException("Department with such ID: " + objectId + " not found in the DB"));
    }
    @Override
    public List<Department> listAll() {
        return departmentRepository.findAll();
    }

    public Page<Department> getAllPaginated(Pageable pageRequest) {
        Page<Department> departments = departmentRepository.findAll(pageRequest);
        return new PageImpl<>(departments.getContent(), pageRequest, departments.getTotalElements());
    }

    public Page<Department> searchDepartments(DepartmentDTO departmentDTO, PageRequest pageRequest) {
        String title = departmentDTO.getTitle().isBlank() ? "%" : departmentDTO.getTitle();
        Page<Department> departments = departmentRepository.searchDepartments(
                pageRequest,
                title
        );
        return new PageImpl<>(departments.getContent(), pageRequest, departments.getTotalElements());
    }
}
