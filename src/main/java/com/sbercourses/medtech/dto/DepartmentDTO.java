package com.sbercourses.medtech.dto;

import com.sbercourses.medtech.model.Department;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString
@NoArgsConstructor
public class DepartmentDTO extends CommonDTO {
    private Long id;
    private String title;
    private String description;

    public DepartmentDTO(final Department department){
        this.id = department.getId();
        this.title = department.getTitle();
        this.description = department.getDescription();
    }
}
