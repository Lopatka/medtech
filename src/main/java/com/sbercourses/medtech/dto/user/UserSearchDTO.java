package com.sbercourses.medtech.dto.user;

import lombok.Data;

@Data
public class UserSearchDTO {
    private String lastName;
    private String firstName;
    private String middleName;
    private String departmentId;
    private String roleId;
}
