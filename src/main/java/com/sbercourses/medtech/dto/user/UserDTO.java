package com.sbercourses.medtech.dto.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sbercourses.medtech.dto.CommonDTO;
import com.sbercourses.medtech.dto.DepartmentDTO;
import com.sbercourses.medtech.dto.RoleDTO;
import com.sbercourses.medtech.model.User;
import com.sbercourses.medtech.model.enums.Gender;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.time.format.DateTimeFormatter;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class UserDTO extends CommonDTO {

    private Long id;
    private RoleDTO role;
    private DepartmentDTO department;
    private String lastName;
    private String firstName;
    private String middleName;
    private Gender gender;
    private String birthDate;
    private String phone;
    private String backUpEmail;
    private String login;
    @JsonIgnore
    private String password;

    public UserDTO(final User user) {
        this.id = user.getId();
        this.role = new RoleDTO(user.getRole());
        this.department = new DepartmentDTO(user.getDepartment());
        this.login = user.getLogin();
        this.firstName = user.getFirstName();
        this.lastName = user.getLastName();
        this.middleName = user.getMiddleName();
        this.gender = user.getGender();
        this.password = user.getPassword();
        this.backUpEmail = user.getBackUpEmail();
        this.phone = user.getPhone();
        this.birthDate = user.getBirthDate().format(DateTimeFormatter.ISO_DATE);
    }
}
