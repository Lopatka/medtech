package com.sbercourses.medtech.dto.user;

import com.sbercourses.medtech.model.Device;
import com.sbercourses.medtech.model.Incident;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class UserInfoDTO extends UserDTO {
    private Set<Device> devices;
    private Set<Incident> incidents;

    public UserInfoDTO(Set<Device> devices){

    }
}
