package com.sbercourses.medtech.dto.incident;

import com.sbercourses.medtech.model.enums.Criticality;
import com.sbercourses.medtech.model.enums.IncidentStatus;
import lombok.Data;

@Data
public class IncidentSearchDTO {
    private String title;
    private Criticality criticality;
    private String createdWhen;
    private IncidentStatus status;
}
