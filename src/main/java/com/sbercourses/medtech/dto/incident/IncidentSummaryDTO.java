package com.sbercourses.medtech.dto.incident;

import com.sbercourses.medtech.model.Detail;
import com.sbercourses.medtech.model.Incident;
import com.sbercourses.medtech.model.IncidentDetail;
import com.sbercourses.medtech.model.enums.Criticality;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class IncidentSummaryDTO {
    private String title;
    private LocalDate createdWhen;
    private LocalDate closedWhen;
    private Criticality criticality;

    private BigDecimal fullPrice = new BigDecimal(0);
    //List<IncidentDetail> details;

    public IncidentSummaryDTO(List<IncidentDetail> incidentDetails) {
        if (!incidentDetails.isEmpty()) {
            Incident incident = incidentDetails.get(0).getIncident();
            this.title = incident.getTitle();
            if (incident.getCreatedWhen() != null) {
                this.createdWhen = incident.getCreatedWhen().toLocalDate();
            }
            if (incident.getClosedWhen() != null) {
                this.closedWhen = incident.getClosedWhen().toLocalDate();
            }
            this.criticality = incident.getCriticality();
            //this.details = incidentDetails;

            for (IncidentDetail incidentDetail : incidentDetails) {
                this.fullPrice = this.fullPrice.add(incidentDetail.getDetail().getPrice()
                        .multiply(BigDecimal.valueOf(incidentDetail.getCount())));
            }
        }
    }
}
