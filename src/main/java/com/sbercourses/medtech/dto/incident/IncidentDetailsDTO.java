package com.sbercourses.medtech.dto.incident;

import com.sbercourses.medtech.dto.detail.DetailDTO;
import com.sbercourses.medtech.model.Detail;
import com.sbercourses.medtech.model.IncidentDetail;
import lombok.*;

import java.util.*;

@Getter
@Setter
@ToString
public class IncidentDetailsDTO {
    private Long id;
    private List<IncidentDetail> detailsIds;
    private String repairDescription;


    public IncidentDetailsDTO(List<IncidentDetail> detailsIds, String repairDescription, Long id) {
        this.id = id;
        this.detailsIds = detailsIds;
        this.repairDescription = repairDescription;
    }
}
