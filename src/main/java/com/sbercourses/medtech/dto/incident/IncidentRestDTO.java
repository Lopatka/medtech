package com.sbercourses.medtech.dto.incident;

import com.sbercourses.medtech.dto.CommonDTO;
import com.sbercourses.medtech.model.Detail;
import com.sbercourses.medtech.model.Incident;
import com.sbercourses.medtech.model.IncidentFile;
import com.sbercourses.medtech.model.enums.Criticality;
import com.sbercourses.medtech.model.enums.IncidentStatus;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;


@Getter
@Setter
@ToString
@NoArgsConstructor
public class IncidentRestDTO extends CommonDTO {
    private Long id;
    private String title;
    private String description;
    private Criticality criticality;
    private Long userId;
    private Long repairerId;
    private Long deviceId;
    private IncidentStatus status;
    private List<Long> incidentDetailsIds = new ArrayList<>();
    private List<Long> incidentFilesIds = new ArrayList<>();

    public IncidentRestDTO(final Incident incident) {
        this.id = incident.getId();
        this.title = incident.getTitle();
        this.description = incident.getDescription();
        this.criticality = incident.getCriticality();
        this.userId = incident.getUser().getId();
        this.deviceId = incident.getDevice().getId();
        this.status = incident.getStatus();
        this.setCreatedWhen(incident.getCreatedWhen());

        if (incident.getRepairer() != null) {
            this.repairerId = incident.getRepairer().getId();
        }

        if (incident.getDetails() != null) {
            for (Detail detail : incident.getDetails()) {
                this.incidentDetailsIds.add(detail.getId());
            }
        }
        if (incident.getIncidentFiles() != null) {
            for (IncidentFile incidentFile : incident.getIncidentFiles()) {
                this.incidentFilesIds.add(incidentFile.getId());
            }
        }
    }
}