package com.sbercourses.medtech.dto.incident;

import com.sbercourses.medtech.dto.CommonDTO;
import com.sbercourses.medtech.dto.device.DeviceDTO;
import com.sbercourses.medtech.dto.user.UserDTO;
import com.sbercourses.medtech.model.Incident;
import com.sbercourses.medtech.model.enums.Criticality;
import com.sbercourses.medtech.model.enums.IncidentStatus;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.Set;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class IncidentDTO extends CommonDTO {
    private Long id;
    private String title;
    private String description;
    private Criticality criticality;
    private UserDTO user;
    private DeviceDTO device;
    private IncidentStatus status;
    private Set<Long> incidentDetailsIds;

    public IncidentDTO(final Incident incident, Set<Long> incidentDetailsIds){
        this.id = incident.getId();
        this.title = incident.getTitle();
        this.description = incident.getDescription();
        this.criticality = incident.getCriticality();
        this.user = new UserDTO(incident.getUser());
        this.device = new DeviceDTO(incident.getDevice());
        this.status = incident.getStatus();
        this.setCreatedWhen(incident.getCreatedWhen());
        this.incidentDetailsIds = incidentDetailsIds;
    }
}
