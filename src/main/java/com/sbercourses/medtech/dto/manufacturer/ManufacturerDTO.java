package com.sbercourses.medtech.dto.manufacturer;

import com.sbercourses.medtech.dto.CommonDTO;
import com.sbercourses.medtech.dto.CountryDTO;
import com.sbercourses.medtech.model.Manufacturer;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class ManufacturerDTO extends CommonDTO {

    private Long id;
    private String title;
    private CountryDTO country;

    public ManufacturerDTO(Manufacturer manufacturer) {
        this.id = manufacturer.getId();
        this.title = manufacturer.getTitle();
        this.country = new CountryDTO(manufacturer.getCountry());
    }
}
