package com.sbercourses.medtech.dto.manufacturer;

import lombok.Data;

@Data
public class ManufacturerSearchDTO {
    private String title;
    private String countryId;
}
