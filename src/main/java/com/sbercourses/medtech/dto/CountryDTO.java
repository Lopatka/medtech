package com.sbercourses.medtech.dto;

import com.sbercourses.medtech.model.Country;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class CountryDTO extends CommonDTO {
    private Long id;
    private String title;

    public CountryDTO(Country country) {
        this.id = country.getId();
        this.title = country.getTitle();
    }
}
