package com.sbercourses.medtech.dto.device;

import com.sbercourses.medtech.dto.incident.IncidentSummaryDTO;
import com.sbercourses.medtech.model.Device;
import com.sbercourses.medtech.model.Incident;
import com.sbercourses.medtech.model.IncidentDetail;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.*;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class DeviceIncidentDTO {
    private List<IncidentSummaryDTO> incidents = new ArrayList<>();

    public DeviceIncidentDTO(List<IncidentDetail> incidentDetails) {
        Map<Long, List<IncidentDetail>> incidentsDetailsMap = new HashMap<>();

        for (IncidentDetail incidentDetail : incidentDetails) {
            Long id = incidentDetail.getIncident().getId();
            if (incidentsDetailsMap.get(id) == null) {
                incidentsDetailsMap.put(incidentDetail.getIncident().getId(), new ArrayList<>());
                incidentsDetailsMap.get(id).add(incidentDetail);
            } else {
                incidentsDetailsMap.get(id).add(incidentDetail);
            }
        }

        for (Map.Entry<Long, List<IncidentDetail>> entry : incidentsDetailsMap.entrySet()) {
            if (entry.getValue() != null) {
                incidents.add(new IncidentSummaryDTO(entry.getValue()));
            }
        }
        sortIncidentSummaryDTO();
    }

    private void sortIncidentSummaryDTO() {
        this.incidents.sort(Comparator.comparing(IncidentSummaryDTO::getCreatedWhen));
    }
}
