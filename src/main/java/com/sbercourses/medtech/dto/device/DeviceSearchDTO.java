package com.sbercourses.medtech.dto.device;

import lombok.Data;

@Data
public class DeviceSearchDTO {
    private String title;
    private String departmentId;
    private String userId;
    private String statusId;
}
