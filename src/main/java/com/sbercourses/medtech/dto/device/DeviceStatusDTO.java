package com.sbercourses.medtech.dto.device;

import com.sbercourses.medtech.model.DeviceStatus;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class DeviceStatusDTO {
    private Long id;
    private String title;
    private String description;


    public DeviceStatusDTO(DeviceStatus status) {
        this.id = status.getId();
        this.title = status.getTitle();
        this.description = status.getDescription();
    }
}
