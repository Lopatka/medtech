package com.sbercourses.medtech.dto.device;

import lombok.Data;

@Data
public class DeviceUserSearchDTO {
    private String userId;
    private String title;
    private String serialNumber;
    private String inventoryNumber;
    private String statusId;
}
