package com.sbercourses.medtech.dto.device;

import com.sbercourses.medtech.dto.CommonDTO;
import com.sbercourses.medtech.dto.DepartmentDTO;
import com.sbercourses.medtech.dto.manufacturer.ManufacturerDTO;
import com.sbercourses.medtech.dto.user.UserDTO;
import com.sbercourses.medtech.model.Device;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class DeviceDTO extends CommonDTO {
    private Long id;
    private String title;
    private String description;
    private DeviceStatusDTO status;
    private UserDTO user;
    private DepartmentDTO department;
    private ManufacturerDTO manufacturer;
    private String model;
    private String serialNumber;
    private String inventoryNumber;
    private String datePurchase;
    private BigDecimal price;

    public DeviceDTO(final Device device){
        this.id = device.getId();
        this.title = device.getTitle();
        this.description = device.getDescription();
        this.status = new DeviceStatusDTO(device.getStatus());
        this.user = new UserDTO(device.getUser());
        this.department = new DepartmentDTO(device.getDepartment());
        this.manufacturer = new ManufacturerDTO(device.getManufacturer());
        this.model = device.getModel();
        this.serialNumber = device.getSerialNumber();
        this.inventoryNumber = device.getInventoryNumber();
        this.datePurchase = device.getDatePurchase().format(DateTimeFormatter.ISO_DATE);
        this.price = device.getPrice();
    }
}
