package com.sbercourses.medtech.dto.detail;

import com.sbercourses.medtech.dto.CommonDTO;
import com.sbercourses.medtech.dto.manufacturer.ManufacturerDTO;
import com.sbercourses.medtech.model.Detail;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class DetailDTO extends CommonDTO {
    private Long id;
    private String title;
    private String description;
    private ManufacturerDTO manufacturer;
    private String serialNumber;
    private String articleNumber;
    private BigDecimal price;
    private Integer amount;
    private String deliveryDate;

    public DetailDTO(Detail detail){
        this.id = detail.getId();
        this.title = detail.getTitle();
        this.description = detail.getDescription();
        this.manufacturer = new ManufacturerDTO(detail.getManufacturer());
        this.serialNumber = detail.getSerialNumber();
        this.articleNumber = detail.getArticleNumber();
        this.price = detail.getPrice();
        this.amount = detail.getAmount();
        this.deliveryDate = detail.getDeliveryDate().format(DateTimeFormatter.ISO_DATE);
    }
}
