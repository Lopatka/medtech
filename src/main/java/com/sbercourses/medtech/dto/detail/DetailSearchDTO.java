package com.sbercourses.medtech.dto.detail;

import lombok.Data;

@Data
public class DetailSearchDTO {
    private String title;
    private String manufacturerId;
    private String articleNumber;
    private String serialNumber;
}
