package com.sbercourses.medtech.MVC.controller;

import com.sbercourses.medtech.dto.device.DeviceUserSearchDTO;
import com.sbercourses.medtech.dto.user.UserDTO;
import com.sbercourses.medtech.dto.user.UserSearchDTO;
import com.sbercourses.medtech.model.Device;
import com.sbercourses.medtech.model.User;
import com.sbercourses.medtech.model.enums.Gender;
import com.sbercourses.medtech.service.*;
import com.sbercourses.medtech.service.userdetails.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import lombok.extern.slf4j.Slf4j;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.EnumSet;

@Controller
@Slf4j
@RequestMapping("/users")
public class MVCUserController {
    @Value("${spring.security.user.roles}")
    private String adminRole;
    private final UserService userService;
    private final RoleService roleService;
    private final DepartmentService departmentService;
    private final DeviceService deviceService;
    private final DeviceStatusService deviceStatusService;

    public MVCUserController(UserService userService,
                             RoleService roleService,
                             DepartmentService departmentService,
                             DeviceService deviceService,
                             DeviceStatusService deviceStatusService) {
        this.userService = userService;
        this.roleService = roleService;
        this.departmentService = departmentService;
        this.deviceService = deviceService;
        this.deviceStatusService = deviceStatusService;
    }

    @GetMapping("")
    public String index(@RequestParam(value = "page", defaultValue = "1") int page,
                        @RequestParam(value = "size", defaultValue = "10") int size,
                        Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, size, Sort.by(Sort.Direction.ASC, "lastName"));
        Page<User> result = userService.getAllPaginated(pageRequest);
        model.addAttribute("users", result);
        model.addAttribute("roles", roleService.listAll());
        model.addAttribute("departments", departmentService.listAll());
        return "users/viewAllUsers";
    }

    @PostMapping("/search")
    public String searchUsers(@RequestParam(value = "page", defaultValue = "1") int page,
                              @RequestParam(value = "size", defaultValue = "10") int size,
                              @ModelAttribute("userSearchForm") @Valid UserSearchDTO userSearchDTO,
                              Model model) {
        model.addAttribute("roles", roleService.listAll());
        model.addAttribute("departments", departmentService.listAll());
        PageRequest pageRequest = PageRequest.of(page - 1, size, Sort.by(Sort.Direction.ASC, "last_name"));
        Page<User> result = userService.searchUsers(userSearchDTO, pageRequest);
        model.addAttribute("users", result);
        return "users/viewAllUsers";
    }

    @GetMapping("/add")
    public String create(Model model) {
        model.addAttribute("roles", roleService.listAll());
        model.addAttribute("departments", departmentService.listAll());
        model.addAttribute("genders", EnumSet.allOf(Gender.class));
        model.addAttribute("userForm", new UserDTO());
        return "users/addUser";
    }

    @PostMapping("/add")
    public String create(@ModelAttribute("userForm") @Valid UserDTO userDTO, Model model) {
        if (userService.createFromDTO(userDTO) == null) {
            model.addAttribute("userForm", userDTO);
            model.addAttribute("roles", roleService.listAll());
            model.addAttribute("departments", departmentService.listAll());
            model.addAttribute("genders", EnumSet.allOf(Gender.class));
            model.addAttribute("userLoginError", "Пользователь с логином: " + userDTO.getLogin() + " уже существует");
            return "users/addUser";
        }
        log.info("CОЗДАНИЕ НОВОГО ПОЛЬЗОВАТЕЛЯ: " + userDTO);
        return "redirect:/users";
    }

    @GetMapping("/profile/{id}")
    public String viewOneUser(@PathVariable Long id,
                              @RequestParam(value = "page", defaultValue = "1") int page,
                              @RequestParam(value = "size", defaultValue = "10") int size,
                              Model model) {
        User user = userService.getOne(id);
        model.addAttribute("user", user);
        PageRequest pageRequest = PageRequest.of(page - 1, size, Sort.by(Sort.Direction.ASC, "title"));
        Page<Device> result = deviceService.getAllPaginated(pageRequest, user.getDevices());
        model.addAttribute("statuses", deviceStatusService.listAll());
        model.addAttribute("devices", result);
        return "users/viewUser";
    }
    @PostMapping("/profile/{id}/search")
    public String searchUserDevices(@PathVariable Long id,
                                    @RequestParam(value = "page", defaultValue = "1") int page,
                                    @RequestParam(value = "size", defaultValue = "10") int size,
                                    @ModelAttribute("userSearchForm") @Valid DeviceUserSearchDTO deviceUserSearchDTO,
                                    Model model) {
        model.addAttribute("user", userService.getOne(id));
        model.addAttribute("statuses", deviceStatusService.listAll());
        PageRequest pageRequest = PageRequest.of(page - 1, size, Sort.by(Sort.Direction.ASC, "title"));
        Page<Device> result = deviceService.searchDevices(deviceUserSearchDTO, pageRequest);
        model.addAttribute("devices", result);
        return "users/viewUser";
    }

    @GetMapping("/profile/{id}/update")
    public String update(@PathVariable Long id,
                             Model model) {
        model.addAttribute("user", userService.getOne(id));
        model.addAttribute("roles", roleService.listAll());
        model.addAttribute("departments", departmentService.listAll());
        model.addAttribute("genders", EnumSet.allOf(Gender.class));
        return "users/updateUser";
    }

    @PostMapping("/profile/{id}/update")
    public String update(@Valid @ModelAttribute("userForm") UserDTO userDTO,
                         Model model,
                         HttpServletRequest request) {
        Long userId = Long.valueOf(request.getParameter("id"));
        if (userService.updateFromDTO(userDTO, userId) == null) {
            model.addAttribute("user", userService.getOne(userId));
            model.addAttribute("roles", roleService.listAll());
            model.addAttribute("departments", departmentService.listAll());
            model.addAttribute("genders", EnumSet.allOf(Gender.class));
            model.addAttribute("userLoginError", "Пользователь с логином: " + userDTO.getLogin() + " уже существует");
            return "users/updateUser";
        }
        log.info("ОБНОВЛЕНИЕ ПРОФИЛЯ ПОЛЬЗОВАТЕЛЯ: " + userDTO);
        return "redirect:/users/profile/" + userDTO.getId();
    }

    @GetMapping("/remember-password")
    public String changePassword() {
        return "users/rememberPassword";
    }

    @PostMapping("/remember-password")
    public String changePassword(@ModelAttribute("changePasswordForm") @Valid UserDTO userDTO) {
        log.info("!!!Changing password!!!!");
        userDTO = userService.getUserByEmail(userDTO.getBackUpEmail());
        userService.sendChangePasswordEmail(userDTO.getBackUpEmail(), userDTO.getId());
        return "redirect:/login";
    }

    @GetMapping("/change-password/{userId}")
    public String changePasswordAfterEmailSent(@PathVariable Long userId, Model model) {
        model.addAttribute("userId", userId);
        return "users/changePassword";
    }

    @PostMapping("/change-password/{userId}")
    public String changePassword(@PathVariable Long userId,
                                 @ModelAttribute("changePasswordForm") @Valid UserDTO userDTO) {
        userService.changePassword(userId, userDTO.getPassword());
        return "redirect:/login";
    }
}

