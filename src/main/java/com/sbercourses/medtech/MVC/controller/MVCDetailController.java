package com.sbercourses.medtech.MVC.controller;

import com.sbercourses.medtech.dto.detail.DetailDTO;
import com.sbercourses.medtech.dto.detail.DetailSearchDTO;
import com.sbercourses.medtech.model.Detail;
import com.sbercourses.medtech.service.DetailService;
import com.sbercourses.medtech.service.ManufacturerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@Slf4j
@PreAuthorize("hasRole('ADMIN') or hasAuthority('ENGINEER')")
@RequestMapping("/details")
public class MVCDetailController {
    private final DetailService detailService;
    private final ManufacturerService manufacturerService;

    public MVCDetailController(DetailService detailService, ManufacturerService manufacturerService) {
        this.detailService = detailService;
        this.manufacturerService = manufacturerService;
    }

    @GetMapping("")
    public String index(@RequestParam(value = "page", defaultValue = "1") int page,
                        @RequestParam(value = "size", defaultValue = "10") int size,
                        Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, size, Sort.by(Sort.Direction.ASC, "title"));
        Page<Detail> result = detailService.getAllPaginated(pageRequest);
        model.addAttribute("manufacturers", manufacturerService.listAll());
        model.addAttribute("details", result);
        return "details/viewAllDetails";
    }

    @PostMapping("/search")
    public String searchDetails(@RequestParam(value = "page", defaultValue = "1") int page,
                                @RequestParam(value = "size", defaultValue = "10") int size,
                                @ModelAttribute("detailSearchForm") @Valid DetailSearchDTO detailSearchDTO,
                                Model model) {
        model.addAttribute("manufacturers", manufacturerService.listAll());
        PageRequest pageRequest = PageRequest.of(page - 1, size, Sort.by(Sort.Direction.ASC, "title"));
        Page<Detail> result = detailService.searchDetails(detailSearchDTO, pageRequest);
        model.addAttribute("details", result);
        return "details/viewAllDetails";
    }

    @GetMapping("/add")
    public String create(Model model) {
        model.addAttribute("manufacturers", manufacturerService.listAll());
        model.addAttribute("details", detailService.listAll());
        model.addAttribute("detailForm", new DetailDTO());
        return "details/addDetail";
    }

    @PostMapping("/add")
    public String create(@ModelAttribute("detailForm") @Valid DetailDTO detailDTO) {
        detailService.createFromDTO(detailDTO);
        log.info("CОЗДАНИЕ НОВОЙ ДЕТАЛИ: " + detailDTO);
        return "redirect:/details";
    }

    @GetMapping("/{id}")
    public String viewOneDetail(@PathVariable Long id,
                                Model model) {
        model.addAttribute("detail", detailService.getOne(id));
        return "details/viewDetail";
    }

    @GetMapping("/{id}/update")
    public String update(@PathVariable Long id,
                         Model model) {
        model.addAttribute("manufacturers", manufacturerService.listAll());
        model.addAttribute("detail", detailService.getOne(id));
        return "details/updateDetail";
    }

    @PostMapping("/{id}/update")
    public String update(@Valid @ModelAttribute("detailForm") DetailDTO detailDTO,
                         Model model,
                         HttpServletRequest request) {
        Long deviceId = Long.valueOf(request.getParameter("id"));
        detailService.updateFromDTO(detailDTO, deviceId);
        log.info("ОБНОВЛЕНИЕ КАРТОЧКИ ДЕТАЛИ: " + detailDTO);
        return "redirect:/details/" + detailDTO.getId();
    }
}
