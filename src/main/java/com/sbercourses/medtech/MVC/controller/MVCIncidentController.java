package com.sbercourses.medtech.MVC.controller;

import com.sbercourses.medtech.dto.incident.IncidentDTO;
import com.sbercourses.medtech.dto.incident.IncidentDetailsDTO;
import com.sbercourses.medtech.dto.incident.IncidentSearchDTO;
import com.sbercourses.medtech.dto.user.UserDTO;
import com.sbercourses.medtech.exception.*;
import com.sbercourses.medtech.model.Detail;
import com.sbercourses.medtech.model.Incident;
import com.sbercourses.medtech.model.IncidentDetail;
import com.sbercourses.medtech.model.IncidentFile;
import com.sbercourses.medtech.model.enums.Criticality;
import com.sbercourses.medtech.model.enums.IncidentStatus;
import com.sbercourses.medtech.model.enums.UserRole;
import com.sbercourses.medtech.service.*;

import com.sbercourses.medtech.service.userdetails.CustomUserDetails;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@Controller
@Slf4j
@RequestMapping("/incidents")
public class MVCIncidentController {
    private final IncidentService incidentService;
    private final DeviceService deviceService;
    private final UserService userService;
    private final DetailService detailService;
    private final ManufacturerService manufacturerService;
    private final IncidentFileService incidentFileService;

    public MVCIncidentController(IncidentService incidentService,
                                 DeviceService deviceService,
                                 UserService userService, DetailService detailService, ManufacturerService manufacturerService, IncidentFileService incidentFileService) {
        this.incidentService = incidentService;
        this.deviceService = deviceService;
        this.userService = userService;
        this.detailService = detailService;
        this.manufacturerService = manufacturerService;
        this.incidentFileService = incidentFileService;
    }

    @GetMapping("")
    public String index(@RequestParam(value = "page", defaultValue = "1") int page,
                        @RequestParam(value = "size", defaultValue = "10") int size,
                        Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        PageRequest pageRequest = PageRequest.of(page - 1, size, Sort.by(Sort.Direction.DESC, "createdWhen"));
        Page<Incident> result = incidentService.getAllPaginated(pageRequest, authentication);
        model.addAttribute("incidents", result);
        model.addAttribute("criticality", EnumSet.allOf(Criticality.class));
        model.addAttribute("statuses", EnumSet.allOf(IncidentStatus.class));
        return "incidents/viewAllIncidents";
    }

    @PostMapping("/search")
    public String searchIncidents(@RequestParam(value = "page", defaultValue = "1") int page,
                                  @RequestParam(value = "size", defaultValue = "10") int size,
                                  @ModelAttribute("incidentSearchForm") @Valid IncidentSearchDTO incidentSearchDTO,
                                  Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        model.addAttribute("criticality", EnumSet.allOf(Criticality.class));
        model.addAttribute("statuses", EnumSet.allOf(IncidentStatus.class));
        PageRequest pageRequest = PageRequest.of(page - 1, size, Sort.by(Sort.Direction.DESC, "created_when"));
        Page<Incident> result = incidentService.searchIncidents(incidentSearchDTO, authentication, pageRequest);
        model.addAttribute("incidents", result);
        return "incidents/viewAllIncidents";
    }

    @PreAuthorize("hasAuthority('MEDICAL_WORKER')")
    @GetMapping("/add")
    public String create(Model model) {
        model.addAttribute("devices", deviceService.listAll());
        model.addAttribute("criticality", EnumSet.allOf(Criticality.class));
        model.addAttribute("incidentForm", new IncidentDTO());
        return "incidents/addIncident";
    }

    @PreAuthorize("hasAuthority('MEDICAL_WORKER')")
    @PostMapping("/add")
    public String create(@ModelAttribute("incidentForm") @Valid IncidentDTO incidentDTO) {
        Long userId = ((CustomUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal()).getUserId().longValue();
        incidentDTO.setUser(new UserDTO(userService.getOne(userId)));
        incidentService.createFromDTO(incidentDTO);
        log.info("CОЗДАНИЕ НОВОЙ ЗАЯВКИ: " + incidentDTO);
        return "redirect:/incidents";
    }

    @PreAuthorize("hasAuthority('ENGINEER')")
    @PostMapping("/{id}/update-details")
    public String updateDetails(@ModelAttribute("incidentDetailsForm") IncidentDetailsDTO incidentDetailsDTO,
                                Model model) {
        try {
            incidentService.updateDetailsFromDTOSecure(incidentDetailsDTO);
        } catch (IncidentUpdateDetailsException exception) {
            return "redirect:/error/errors?message=" + exception.getMessage();
        }
        return "redirect:/incidents/" + incidentDetailsDTO.getId();
    }

    @PostMapping("/{id}/update-files")
    public String updateFiles(@RequestParam("files") MultipartFile[] files, Model model, @PathVariable Long id) {
        try {
            incidentFileService.createFilesSecure(files, id);
        } catch (IncidentCreateFilesException exception) {
            return "redirect:/error/errors?message=" + exception.getMessage();
        }
        return "redirect:/incidents/" + id;
    }

    @GetMapping("/{id}/delete-file/{fileId}")
    public String deleteFile(@PathVariable Long fileId, @PathVariable Long id) {
        try {
            incidentFileService.deleteFileSecure(fileId, id);
        } catch (IncidentDeleteFileException exception) {
            return "redirect:/error/errors?message=" + exception.getMessage();
        }
        return "redirect:/incidents/" + id;
    }

    @GetMapping("/{id}/delete-all-files")
    public String deleteAllFiles(@PathVariable Long id) {
        try {
            incidentFileService.deleteAllFilesSecure(id);
        } catch (IncidentDeleteFileException exception) {
            return "redirect:/error/errors?message=" + exception.getMessage();
        }
        return "redirect:/incidents/" + id;
    }

    @GetMapping(value = "/download", produces = MediaType.MULTIPART_FORM_DATA_VALUE)
    @ResponseBody
    public ResponseEntity<Resource> downloadFile(@Param(value = "incidentId") Long incidentId,
                                                 @Param(value = "fileId") Long fileId) throws IOException, IncidentGetOneException {
        IncidentFile incidentFile = incidentFileService.getOneSecure(incidentId, fileId);
        Path path = Paths.get(incidentFile.getPath());
        ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));

        return ResponseEntity.ok()
                .headers(this.headers(path.getFileName().toString()))
                .contentLength(path.toFile().length())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(resource);
    }

    private HttpHeaders headers(String name) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + name);
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        return headers;
    }

    @GetMapping("/{id}")
    public String viewOneIncident(@PathVariable Long id,
                                  Model model) {
        Incident incident = incidentService.getOne(id);
        List<IncidentDetail> incidentDetailsIds = detailService.findIncidentDetails(id);
        model.addAttribute("incidentDetailsForm", new IncidentDetailsDTO(incidentDetailsIds, incident.getRepairDescription(), id));
        model.addAttribute("incidentFilesForm", new IncidentFile());
        model.addAttribute("filesAll", incidentFileService.listAllById(id));
        model.addAttribute("incident", incident);
        if (SecurityContextHolder.getContext().getAuthentication().
                getAuthorities().stream().anyMatch(a -> a.getAuthority().equals(UserRole.ENGINEER.name()))) {
            List<Detail> details = detailService.listAll();
            details.removeAll(incident.getDetails());
            model.addAttribute("details", details);
            model.addAttribute("manufacturers", manufacturerService.listAll());
        }
        return "incidents/viewIncident";
    }

    @PreAuthorize("hasAuthority('MEDICAL_WORKER') || hasAuthority('ENGINEER')")
    @PostMapping("/update-status/{id}")
    public String updateStatus(@PathVariable Long id,
                               @ModelAttribute("incidentStatus") IncidentStatus incidentStatus) {
        try {
            Incident incident = incidentService.updateStatus(incidentStatus, id);
            incidentService.update(incident);
        } catch (IncidentUpdateStatusException exception) {
            return "redirect:/error/errors?message=" + exception.getMessage();
        }
        return "redirect:/incidents";
    }

    @PreAuthorize("hasAuthority('MEDICAL_WORKER')")
    @GetMapping("/{id}/update")
    public String update(@PathVariable Long id,
                         Model model) {
        model.addAttribute("devices", deviceService.listAll());
        model.addAttribute("criticality", EnumSet.allOf(Criticality.class));
        model.addAttribute("incident", incidentService.getOne(id));
        return "incidents/updateIncident";
    }

    @PreAuthorize("hasAuthority('MEDICAL_WORKER')")
    @PostMapping("/{id}/update")
    public String update(@Valid @ModelAttribute("incidentForm") IncidentDTO incidentDTO,
                         @PathVariable Long id,
                         HttpServletRequest request){
        try {
            incidentService.updateFromDTOSecure(incidentDTO, id);
            log.info("ОБНОВЛЕНИЕ КАРТОЧКИ ЗАЯВКИ: " + incidentDTO);
        } catch (IncidentUpdateException exception) {
            return "redirect:/error/errors?message=" + exception.getMessage();
        }
        return "redirect:/incidents/" + incidentDTO.getId();
    }

    @GetMapping("delete/{id}")
    public String delete(@PathVariable Long id) throws IncidentDeleteFileException {
        Incident incident = incidentService.getOne(id);

        if (SecurityContextHolder.getContext().getAuthentication().
                getAuthorities().stream().anyMatch(a -> a.getAuthority().equals(UserRole.ADMIN.getRoleName()))) {
            incidentFileService.deleteAllFiles(id);
            incidentService.delete(id);
            return "redirect:/incidents";
        }

        Long userId = ((CustomUserDetails) SecurityContextHolder.getContext().getAuthentication()
                .getPrincipal()).getUserId().longValue();
        if (incident.getStatus().equals(IncidentStatus.DRAFT) && (incident.getUser().getId().equals(userId))) {
            incidentFileService.deleteAllFilesSecure(id);
            incidentService.delete(id);
        }
        return "redirect:/incidents";
    }
}
