package com.sbercourses.medtech.MVC.controller;

import com.sbercourses.medtech.dto.DepartmentDTO;
import com.sbercourses.medtech.model.Department;
import com.sbercourses.medtech.service.DepartmentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@Slf4j
@RequestMapping("/departments")
public class MVCDepartmentController {
    private final DepartmentService departmentService;

    public MVCDepartmentController(DepartmentService departmentService) {
        this.departmentService = departmentService;
    }

    @GetMapping("")
    public String index(@RequestParam(value = "page", defaultValue = "1") int page,
                        @RequestParam(value = "size", defaultValue = "10") int size,
                        Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, size, Sort.by(Sort.Direction.ASC, "title"));
        Page<Department> result = departmentService.getAllPaginated(pageRequest);
        model.addAttribute("departments", result);
        return "departments/viewAllDepartments";
    }

    @PostMapping("/search")
    public String searchDepartments(@RequestParam(value = "page", defaultValue = "1") int page,
                                    @RequestParam(value = "size", defaultValue = "10") int size,
                                    @ModelAttribute("departmentSearchForm") @Valid DepartmentDTO departmentDTO,
                                    Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, size, Sort.by(Sort.Direction.ASC, "title"));
        Page<Department> result = departmentService.searchDepartments(departmentDTO, pageRequest);
        model.addAttribute("departments", result);
        return "departments/viewAllDepartments";
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/add")
    public String create(Model model) {
        return "departments/addDepartment";
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/add")
    public String create(@ModelAttribute("departmentForm") @Valid DepartmentDTO departmentDTO) {
        departmentService.createFromDTO(departmentDTO);
        return "redirect:/departments";
    }

    @GetMapping("/{id}")
    public String viewOneDepartment(@PathVariable Long id,
                                Model model) {
        model.addAttribute("department", departmentService.getOne(id));
        return "departments/viewDepartment";
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/{id}/update")
    public String update(@PathVariable Long id,
                         Model model) {
        model.addAttribute("department", departmentService.getOne(id));
        return "departments/updateDepartment";
    }
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/{id}/update")
    public String update(@Valid @ModelAttribute("deviceForm") DepartmentDTO departmentDTO,
                         Model model,
                         HttpServletRequest request) {
        Long departmentId = Long.valueOf(request.getParameter("id"));
        departmentService.updateFromDTO(departmentDTO, departmentId);
        log.info("ОБНОВЛЕНИЕ КАРТОЧКИ ПОДРАЗДЕЛЕНИЯ: " + departmentDTO);
        return "redirect:/departments/" + departmentDTO.getId();
    }
}
