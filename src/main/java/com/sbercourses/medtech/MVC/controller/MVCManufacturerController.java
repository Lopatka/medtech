package com.sbercourses.medtech.MVC.controller;

import com.sbercourses.medtech.dto.manufacturer.ManufacturerDTO;
import com.sbercourses.medtech.dto.manufacturer.ManufacturerSearchDTO;
import com.sbercourses.medtech.model.Manufacturer;
import com.sbercourses.medtech.service.CountryService;
import com.sbercourses.medtech.service.ManufacturerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@Controller
@Slf4j
@PreAuthorize("hasRole('ADMIN') or hasAuthority('ENGINEER')")
@RequestMapping("/manufacturers")
public class MVCManufacturerController {
    private final ManufacturerService manufacturerService;
    private final CountryService countryService;

    public MVCManufacturerController(ManufacturerService manufacturerService, CountryService countryService) {
        this.manufacturerService = manufacturerService;
        this.countryService = countryService;
    }

    @GetMapping("")
    public String index(@RequestParam(value = "page", defaultValue = "1") int page,
                        @RequestParam(value = "size", defaultValue = "10") int size,
                        Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, size, Sort.by(Sort.Direction.ASC, "title"));
        Page<Manufacturer> result = manufacturerService.getAllPaginated(pageRequest);
        model.addAttribute("countries", countryService.listAll());
        model.addAttribute("manufacturers", result);
        return "manufacturers/viewAllManufacturers";
    }
    @PreAuthorize("hasRole('ADMIN') or hasAuthority('ENGINEER')")
    @PostMapping("/search")
    public String searchManufacturers(@RequestParam(value = "page", defaultValue = "1") int page,
                                      @RequestParam(value = "size", defaultValue = "10") int size,
                                      @ModelAttribute("manufacturerSearchForm") @Valid ManufacturerSearchDTO manufacturerSearchDTO,
                                      Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, size, Sort.by(Sort.Direction.ASC, "title"));
        Page<Manufacturer> result = manufacturerService.searchManufacturers(manufacturerSearchDTO, pageRequest);
        model.addAttribute("countries", countryService.listAll());
        model.addAttribute("manufacturers", result);
        return "manufacturers/viewAllManufacturers";
    }

    @GetMapping("/add")
    public String create(Model model) {
        model.addAttribute("countries", countryService.listAll());
        model.addAttribute("manufacturerForm", new ManufacturerDTO());
        return "manufacturers/addManufacturer";
    }

    @PostMapping("/add")
    public String create(@ModelAttribute("manufacturerForm") @Valid ManufacturerDTO manufacturerDTO) {
        manufacturerService.createFromDTO(manufacturerDTO);
        log.info("CОЗДАНИЕ НОВОГО ПРОИЗВОДИТЕЛЯ: " + manufacturerDTO);
        return "redirect:/manufacturers";
    }

    @GetMapping("/{id}")
    public String viewOneManufacturer(@PathVariable Long id,
                                Model model) {
        model.addAttribute("manufacturer", manufacturerService.getOne(id));
        return "manufacturers/viewManufacturer";
    }

    @GetMapping("/{id}/update")
    public String update(@PathVariable Long id,
                         Model model) {
        model.addAttribute("countries", countryService.listAll());
        model.addAttribute("manufacturer", manufacturerService.getOne(id));
        return "manufacturers/updateManufacturer";
    }

    @PostMapping("/{id}/update")
    public String update(@ModelAttribute("manufacturerForm") @Valid ManufacturerDTO manufacturerDTO,
                         Model model,
                         HttpServletRequest request) {
        Long manufacturerId = Long.valueOf(request.getParameter("id"));
        manufacturerService.updateFromDTO(manufacturerDTO, manufacturerId);
        log.info("ОБНОВЛЕНИЕ КАРТОЧКИ ПРОИЗВОДИТЕЛЯ: " + manufacturerDTO);
        return "redirect:/manufacturers/" + manufacturerDTO.getId();
    }
}
