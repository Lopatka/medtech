package com.sbercourses.medtech.MVC.controller;

import com.sbercourses.medtech.dto.device.DeviceDTO;
import com.sbercourses.medtech.dto.device.DeviceSearchDTO;
import com.sbercourses.medtech.model.Device;
import com.sbercourses.medtech.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;


@Controller
@Slf4j
@RequestMapping("/devices")
public class MVCDeviceController {

    private final DeviceService deviceService;
    private final UserService userService;
    private final DepartmentService departmentService;
    private final DeviceStatusService deviceStatusService;
    private final ManufacturerService manufacturerService;

    public MVCDeviceController(DeviceService deviceService, UserService userService, DepartmentService departmentService, DeviceStatusService deviceStatusService, ManufacturerService manufacturerService) {
        this.deviceService = deviceService;
        this.userService = userService;
        this.departmentService = departmentService;
        this.deviceStatusService = deviceStatusService;
        this.manufacturerService = manufacturerService;
    }

    @PreAuthorize("hasRole('ADMIN') or hasAuthority('ENGINEER')")
    @GetMapping("")
    public String index(@RequestParam(value = "page", defaultValue = "1") int page,
                        @RequestParam(value = "size", defaultValue = "10") int size,
                        Model model) {
        PageRequest pageRequest = PageRequest.of(page - 1, size, Sort.by(Sort.Direction.ASC, "title"));
        Page<Device> result = deviceService.getAllPaginated(pageRequest);
        model.addAttribute("devices", result);
        model.addAttribute("departments", departmentService.listAll());
        model.addAttribute("users", userService.listAll());
        model.addAttribute("statuses", deviceStatusService.listAll());
        return "devices/viewAllDevices";
    }

    @PreAuthorize("hasRole('ADMIN') or hasAuthority('ENGINEER')")
    @PostMapping("/search")
    public String searchDevices(@RequestParam(value = "page", defaultValue = "1") int page,
                                @RequestParam(value = "size", defaultValue = "10") int size,
                                @ModelAttribute("deviceSearchForm") @Valid DeviceSearchDTO deviceSearchDTO,
                                Model model) {
        model.addAttribute("departments", departmentService.listAll());
        model.addAttribute("users", userService.listAll());
        model.addAttribute("statuses", deviceStatusService.listAll());
        PageRequest pageRequest = PageRequest.of(page - 1, size, Sort.by(Sort.Direction.ASC, "title"));
        Page<Device> result = deviceService.searchDevices(deviceSearchDTO, pageRequest);
        model.addAttribute("devices", result);
        return "devices/viewAllDevices";
    }

    @PreAuthorize("hasRole('ADMIN') or hasAuthority('ENGINEER')")
    @GetMapping("/add")
    public String create(Model model) {
        model.addAttribute("manufacturers", manufacturerService.listAll());
        model.addAttribute("users", userService.listAll());
        model.addAttribute("departments", departmentService.listAll());
        model.addAttribute("statuses", deviceStatusService.listAll());
        model.addAttribute("deviceForm", new DeviceDTO());
        return "devices/addDevice";
    }

    @PreAuthorize("hasRole('ADMIN') or hasAuthority('ENGINEER')")
    @PostMapping("/add")
    public String create(@ModelAttribute("deviceForm") @Valid DeviceDTO deviceDTO) {
        deviceService.createFromDTO(deviceDTO);
        log.info("CОЗДАНИЕ НОВОГО ПОЛЬЗОВАТЕЛЯ: " + deviceDTO);
        return "redirect:/devices";
    }

    @GetMapping("/{id}")
    public String viewOneDevice(@PathVariable Long id,
                              Model model) {
        model.addAttribute("device", deviceService.getOne(id));
        return "devices/viewDevice";
    }

    @PreAuthorize("hasRole('ADMIN') or hasAuthority('ENGINEER')")
    @GetMapping("/{id}/update")
    public String update(@PathVariable Long id,
                         Model model) {
        model.addAttribute("manufacturers", manufacturerService.listAll());
        model.addAttribute("users", userService.listAll());
        model.addAttribute("device", deviceService.getOne(id));
        model.addAttribute("departments", departmentService.listAll());
        model.addAttribute("statuses", deviceStatusService.listAll());
        return "devices/updateDevice";
    }

    @PreAuthorize("hasRole('ADMIN') or hasAuthority('ENGINEER')")
    @PostMapping("/{id}/update")
    public String update(@Valid @ModelAttribute("deviceForm") DeviceDTO deviceDTO,
                         Model model,
                         HttpServletRequest request) {
        Long deviceId = Long.valueOf(request.getParameter("id"));
        deviceService.updateFromDTO(deviceDTO, deviceId);
        log.info("ОБНОВЛЕНИЕ КАРТОЧКИ ОБОРУДОВАНИЯ: " + deviceDTO);
        return "redirect:/devices/" + deviceDTO.getId();
    }

    @PreAuthorize("hasRole('ADMIN') or hasAuthority('ENGINEER')")
    @GetMapping("delete/{id}")
    public String delete(@PathVariable Long id) {
        deviceService.delete(id);
        return "redirect:/devices";
    }
}
