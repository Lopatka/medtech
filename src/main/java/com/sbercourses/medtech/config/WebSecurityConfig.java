package com.sbercourses.medtech.config;

import com.sbercourses.medtech.service.userdetails.CustomUserDetails;
import com.sbercourses.medtech.service.userdetails.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.firewall.HttpFirewall;
import org.springframework.security.web.firewall.StrictHttpFirewall;

import java.util.Objects;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity
public class WebSecurityConfig {

    private CustomUserDetailsService userService;
    private final String roleAdmin = "ROLE_ADMIN";

    private static final String[] AUTH_WHITELIST = {
            //  -- Swagger UI v2
            "/v2/api-docs",
            "/swagger-resources",
            "/swagger-resources/**",
            "/configuration/ui",
            "/configuration/security",
            "/swagger-ui.html",
            "/swagger-ui.html/**",
            "/webjars/**",
            // -- Swagger UI v3 (OpenAPI)
            "/v3/api-docs/**",
            "/swagger-ui/**",
            "/css/**",
            "/img/**",
            "/js/**",
            "/encode/*",
            "/login"
            // other public endpoints of your API may be appended to this array
    };

    public WebSecurityConfig(CustomUserDetailsService userService) {
        this.userService = userService;
    }

    public boolean isAdmin(Authentication authentication){
        return authentication.getAuthorities().contains(new SimpleGrantedAuthority(roleAdmin));
    }

    public boolean checkUserId(Long id) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (isAdmin(authentication)){
            return true;
        }
        CustomUserDetails userDetails = (CustomUserDetails) authentication.getPrincipal();
        return Objects.equals(Long.valueOf(userDetails.getUserId()), id);
    }

    //Создаем бин нашего энкриптора паролей
    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    public HttpFirewall allowPercent() {
        StrictHttpFirewall firewall = new StrictHttpFirewall();
        firewall.setAllowUrlEncodedPercent(true);
        firewall.setAllowUrlEncodedSlash(true);
        return firewall;
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
                //Доступ для всех пользователей
                .antMatchers("/login", "/users/remember-password", "/users/change-password/**")
                .permitAll()
                .and()
                .authorizeRequests()
                .antMatchers(AUTH_WHITELIST).permitAll()
                .antMatchers("/resources/**",
                        "/static/**",
                        "/assets/**",
                        "/css/**",
                        "/js/**",
                        "/image/**",
                        "/webjars/**"
                )
                .permitAll()
                .antMatchers("/users",
                        "/users/add",
                        "/users/search",
                        "/departments/add",
                        "/departments/{id}/update")
                .hasRole("ADMIN")
                .antMatchers("/users/profile/{id}/**")
                .access("@webSecurityConfig.checkUserId(#id)")
                //все остальные страницы требуют авторизации
                .anyRequest().authenticated()
                .and()
                //Настройка для входа в систему
                .formLogin()
                .loginPage("/login")
                //Перенаправление на главную страницу после успешного входа
                .defaultSuccessUrl("/")
                .permitAll()
                .and()
                .logout()
                .permitAll()
                .logoutSuccessUrl("/login");
        return http.build();
    }

    @Autowired
    protected void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userService).passwordEncoder(bCryptPasswordEncoder());
    }
}

