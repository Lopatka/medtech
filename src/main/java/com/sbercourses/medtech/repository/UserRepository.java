package com.sbercourses.medtech.repository;

import com.sbercourses.medtech.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findByBackUpEmail(String email);

    User findUserByLogin(String username);

    @Query(nativeQuery = true,
            value = "select * from users u \n" +
                    "join roles r on u.role_id = r.id\n" +
                    "join departments d on u.department_id  = d.id\n" +
                    "where lower(u.last_name) like '%' || lower(:lastName) || '%'\n" +
                    "and lower(u.first_name) like '%' || lower(:firstName) || '%'\n" +
                    "and coalesce(lower(u.middle_name), '%') like '%' || lower(:middleName) || '%'\n" +
                    "and cast(d.id as varchar) like coalesce(:departmentId, '%') \n" +
                    "and cast(r.id as varchar) like coalesce(:roleId, '%') ",
            countQuery = "select count(*) from users u \n" +
                    "join roles r on u.role_id = r.id\n" +
                    "join departments d on u.department_id  = d.id\n" +
                    "where lower(u.last_name) like '%' || lower(:lastName) || '%'\n" +
                    "and lower(u.first_name) like '%' || lower(:firstName) || '%'\n" +
                    "and coalesce(lower(u.middle_name), '%') like '%' || lower(:middleName) || '%'\n" +
                    "and cast(d.id as varchar) like coalesce(:departmentId, '%') \n" +
                    "and cast(r.id as varchar) like coalesce(:roleId, '%') ")
    Page<User> searchUsers(Pageable pageable,
                           @Param(value = "lastName") String lastName,
                           @Param(value = "firstName") String firstName,
                           @Param(value = "middleName") String middleName,
                           @Param(value = "departmentId") String departmentId,
                           @Param(value = "roleId") String roleId);
}
