package com.sbercourses.medtech.repository;

import com.sbercourses.medtech.model.Incident;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface IncidentRepository extends JpaRepository<Incident, Long> {
    @Query(nativeQuery = true,
    value = "select * from incidents i \n" +
            "left join users u on i.user_id = u.id \n" +
            "left join users ur on i.repairer_id = ur.id \n" +
            "where lower(i.title) like '%' || lower(:title) || '%'\n" +
            "and lower(i.criticality) like '%' || lower(:criticality) || '%'\n" +
            "and lower(i.status) like '%' || lower(:status) || '%'\n" +
            "and cast(i.created_when as varchar) like '%' || lower(:createdWhen) || '%'\n" +
            "and ((i.user_id = :userId or i.repairer_id  = :repairerId) or ((:userId is null) and (:repairerId is null)))",
    countQuery = "select count(*) from incidents i \n" +
            "left join users u on i.user_id = u.id \n" +
            "left join users ur on i.repairer_id = ur.id \n" +
            "where lower(i.title) like '%' || lower(:title) || '%'\n" +
            "and lower(i.criticality) like '%' || lower(:criticality) || '%'\n" +
            "and lower(i.status) like '%' || lower(:status) || '%'\n" +
            "and cast(i.created_when as varchar) like '%' || lower(:createdWhen) || '%'\n" +
            "and ((i.user_id = :userId or i.repairer_id  = :repairerId) or ((:userId is null) and (:repairerId is null)))")
    Page<Incident> searchIncidents(PageRequest pageRequest,
                                   @Param(value = "title") String title,
                                   @Param(value = "criticality") String criticality,
                                   @Param(value = "createdWhen") String createdWhen,
                                   @Param(value = "status") String status,
                                   @Param(value = "userId") Long userId,
                                   @Param(value = "repairerId") Long repairerId);
    @Query(nativeQuery = true,
    value = "select * from incidents i \n" +
            "left join users ur on i.repairer_id = ur.id \n" +
            "where i.repairer_id = :repairerId or i.status != 'DRAFT'",
    countQuery = "select count(*) from incidents i \n" +
            "left join users ur on i.repairer_id = ur.id \n" +
            "where i.repairer_id = :repairerId or i.status != 'DRAFT'")
    List<Incident> findAllByRepairerId(Long repairerId);

    List<Incident> findAllByUserId(Long userId);

    List<Incident> findAllByDeviceId(Long deviceId);
}
