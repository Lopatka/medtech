package com.sbercourses.medtech.repository;

import com.sbercourses.medtech.model.Device;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DeviceRepository extends JpaRepository<Device, Long> {
    @Query(nativeQuery = true,
            value = "select * from devices d \n" +
                    "where lower(d.title) like '%' || lower(:title) || '%'\n" +
                    "and cast(d.department_id as varchar) like coalesce(:departmentId, '%') \n" +
                    "and cast(d.user_id as varchar) like coalesce(:userId, '%') \n" +
                    "and cast(d.status_id as varchar) like coalesce(:statusId, '%')",
            countQuery =
                    "select count(*) from devices d \n" +
                    "where lower(d.title) like '%' || lower(:title) || '%'\n" +
                    "and cast(d.department_id as varchar) like coalesce(:departmentId, '%') \n" +
                    "and cast(d.user_id as varchar) like coalesce(:userId, '%') \n" +
                    "and cast(d.status_id as varchar) like coalesce(:statusId, '%')")
    Page<Device> searchDevices(Pageable pageable,
                               @Param(value = "title") String title,
                               @Param(value = "departmentId") String departmentId,
                               @Param(value = "userId") String userId,
                               @Param(value = "statusId") String statusId);

    @Query(nativeQuery = true,
            value = "select * from devices d \n" +
                    "where cast(d.user_id as varchar) like coalesce(:userId, '%') \n" +
                    "and lower(d.title) like '%' || lower(:title) || '%'\n" +
                    "and lower(d.serial_number) like '%' || lower(:serialNumber) || '%'\n" +
                    "and lower(d.inventory_number) like '%' || lower(:inventoryNumber) || '%'\n" +
                    "and cast(d.status_id as varchar) like coalesce(:statusId, '%')",
            countQuery = "select count(*) from devices d \n" +
                    "where cast(d.user_id as varchar) like coalesce(:userId, '%') \n" +
                    "and lower(d.title) like '%' || lower(:title) || '%'\n" +
                    "and lower(d.serial_number) like '%' || lower(:serialNumber) || '%'\n" +
                    "and lower(d.inventory_number) like '%' || lower(:inventoryNumber) || '%'\n" +
                    "and cast(d.status_id as varchar) like coalesce(:statusId, '%')")
    Page<Device> searchDevices(PageRequest pageRequest,
                               @Param(value = "userId") String userId,
                               @Param(value = "title") String title,
                               @Param(value = "serialNumber") String serialNumber,
                               @Param(value = "inventoryNumber") String inventoryNumber,
                               @Param(value = "statusId") String statusId);
}
