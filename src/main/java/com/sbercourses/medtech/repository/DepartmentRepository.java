package com.sbercourses.medtech.repository;

import com.sbercourses.medtech.model.Department;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartmentRepository extends JpaRepository<Department, Long> {
    @Query(nativeQuery = true,
    value = "select * from departments d \n" +
            "where lower(d.title) like '%' || lower(:title) || '%'",
    countQuery = "select count(*) from departments d \n" +
            "where lower(d.title) like '%' || lower(:title) || '%'")
    Page<Department> searchDepartments(PageRequest pageRequest, @Param(value = "title") String title);
}
