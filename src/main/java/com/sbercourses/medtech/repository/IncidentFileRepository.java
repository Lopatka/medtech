package com.sbercourses.medtech.repository;

import com.sbercourses.medtech.model.IncidentFile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IncidentFileRepository extends JpaRepository<IncidentFile, Long> {
    List<IncidentFile> findAllByIncidentId(Long id);

    IncidentFile findByFileNameAndIncidentId(String fileName, Long id);
}
