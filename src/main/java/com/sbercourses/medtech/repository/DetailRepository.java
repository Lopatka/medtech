package com.sbercourses.medtech.repository;

import com.sbercourses.medtech.model.Detail;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface DetailRepository extends JpaRepository<Detail, Long> {
    @Query(nativeQuery = true,
    value = "select d.* from details d \n" +
            "join manufacturers m  on d.manufacturer_id = m.id\n" +
            "where lower(d.title) like '%' || lower(:title) || '%'\n" +
            "and cast(d.manufacturer_id as varchar) like coalesce(:manufacturerId, '%') \n" +
            "and lower(d.article_number) like '%' || lower(:articleNumber) || '%'\n" +
            "and lower(d.serial_number) like '%' || lower(:serialNumber) || '%'",
    countQuery = "select count(*) from details d \n" +
            "join manufacturers m  on d.manufacturer_id = m.id\n" +
            "where lower(d.title) like '%' || lower(:title) || '%'\n" +
            "and cast(d.manufacturer_id as varchar) like coalesce(:manufacturerId, '%') \n" +
            "and lower(d.article_number) like '%' || lower(:articleNumber) || '%'\n" +
            "and lower(d.serial_number) like '%' || lower(:serialNumber) || '%'")
    Page<Detail> searchDetails(PageRequest pageRequest,
                               @Param(value = "title") String title,
                               @Param(value = "manufacturerId") String manufacturerId,
                               @Param(value = "articleNumber") String articleNumber,
                               @Param(value = "serialNumber") String serialNumber);
}
