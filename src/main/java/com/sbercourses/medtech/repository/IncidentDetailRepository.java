package com.sbercourses.medtech.repository;

import com.sbercourses.medtech.model.IncidentDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IncidentDetailRepository extends JpaRepository<IncidentDetail, Long> {

    @Query(nativeQuery = true,
            value = "select * from details d\n" +
                    "join incidents_details id on d.id = id.detail_id \n" +
                    "where id.incident_id = :incidentId")
    List<IncidentDetail> findDetailsByIncidentId(@Param(value = "incidentId") Long incidentId);
}
