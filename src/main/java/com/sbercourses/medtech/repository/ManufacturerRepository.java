package com.sbercourses.medtech.repository;

import com.sbercourses.medtech.model.Manufacturer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ManufacturerRepository extends JpaRepository<Manufacturer, Long> {
    @Query(nativeQuery = true,
            value = "select * from manufacturers m \n" +
                    "left join countries c on m.country_id = c.id \n" +
                    "where lower(m.title) like '%' || lower(:manufacturerTitle) || '%' \n" +
                    "and cast(c.id as varchar) like coalesce(:countryId, '%')",
            countQuery = "select count(*) from manufacturers m \n" +
                    "left join countries c on m.country_id = c.id \n" +
                    "where lower(m.title) like '%' || lower(:manufacturerTitle) || '%' \n" +
                    "and cast(c.id as varchar) like coalesce(:countryId, '%')")
    Page<Manufacturer> searchManufacturers(PageRequest pageRequest,
                                       @Param(value = "manufacturerTitle") String manufacturerTitle,
                                       @Param(value = "countryId") String countryId);
}
