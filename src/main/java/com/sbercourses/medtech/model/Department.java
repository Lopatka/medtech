package com.sbercourses.medtech.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "departments")
@NoArgsConstructor
@Getter
@Setter
@SequenceGenerator(name = "default_gen", sequenceName = "departments_seq", allocationSize = 1)
public class Department extends GenericModel {

    @Column(name = "title", nullable = false)
    private String title;

    @Column(columnDefinition="TEXT", name = "description")
    private String description;
}





