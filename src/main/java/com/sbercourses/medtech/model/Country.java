package com.sbercourses.medtech.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "countries")
@NoArgsConstructor
@Getter
@Setter
@SequenceGenerator(name = "default_gen", sequenceName = "countries_seq", allocationSize = 1)
public class Country extends GenericModel {

    @Column(name = "title", nullable = false)
    private String title;
}
