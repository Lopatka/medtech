package com.sbercourses.medtech.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;

@Entity
@Table(name = "devices")
@NoArgsConstructor
@Getter
@Setter
@Builder
@AllArgsConstructor
@SequenceGenerator(name = "default_gen", sequenceName = "devices_seq", allocationSize = 1)
public class Device extends GenericModel {

    @Column(name = "title", nullable = false)
    private String title;

    @Column(columnDefinition="TEXT", name = "description")
    private String description;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "status_id", foreignKey = @ForeignKey(name = "FK_DEVICE_STATUS"), nullable = false)
    private DeviceStatus status;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "FK_DEVICE_USER"), nullable = false)
    private User user;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "department_id", foreignKey = @ForeignKey(name = "FK_DEVICE_DEPARTMENT"), nullable = false)
    private Department department;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "manufacturer_id", foreignKey = @ForeignKey(name = "FK_DEVICE_MANUFACTURER"), nullable = false)
    private Manufacturer manufacturer;

    @Column(name = "model")
    private String model;

    @Column(name = "serial_number")
    private String serialNumber;

    @Column(name = "inventory_number")
    private String inventoryNumber;

    @Column(name = "date_purchase")
    private LocalDate datePurchase;

    @Column(name = "price")
    private BigDecimal price;

    @OneToMany(mappedBy = "device", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JsonIgnore
    private Set<Incident> incidents;
}
