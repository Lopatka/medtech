package com.sbercourses.medtech.model.enums;

public enum Gender {
    MALE("Мужской"),
    FEMALE("Женский");

    private final String genderName;

    Gender(String genderName) {
        this.genderName = genderName;
    }

    public String getGenderName() {
        return this.genderName;
    }
}
