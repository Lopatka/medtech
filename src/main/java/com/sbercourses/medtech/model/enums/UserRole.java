package com.sbercourses.medtech.model.enums;

public enum UserRole {
    MEDICAL_WORKER("ROLE_MEDICAL_WORKER"),
    ENGINEER("ROLE_ENGINEER"),
    ADMIN("ROLE_ADMIN");

    private final String roleName;

    UserRole(String roleName) {
        this.roleName = roleName;
    }
    public String getRoleName(){
        return this.roleName;
    }
}
