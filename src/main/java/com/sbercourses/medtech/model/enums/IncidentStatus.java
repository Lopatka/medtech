package com.sbercourses.medtech.model.enums;

public enum IncidentStatus {
    DRAFT("Черновик"),
    NEW("Новая"),
    WORKING("Устранение инженером"),
    COMPLETED("Выполнена"),
    CLOSED("Закрыта");

    private final String incidentStatusName;

    IncidentStatus(String incidentStatusName){
        this.incidentStatusName = incidentStatusName;
    }

    public String getIncidentStatusName(){
        return this.incidentStatusName;
    }
}
