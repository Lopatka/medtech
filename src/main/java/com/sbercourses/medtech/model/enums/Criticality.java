package com.sbercourses.medtech.model.enums;

public enum Criticality {
    LOW("Низкая"),
    MEDIUM("Средняя"),
    HIGH("Высокая");



    private final String criticalityName;
    private String color;

    Criticality(String criticalityName) {
        switch (criticalityName) {
            case "Высокая":
                this.color = "red";
                break;
            case "Средняя":
                this.color = "yellow";
                break;
            case "Низкая":
                this.color = "green";
                break;
            default:
                this.color = "white";
                break;
        }
        this.criticalityName = criticalityName;
    }

    public String getCriticalityName() {
        return this.criticalityName;
    }

    public String getCriticalityColor() {
        return this.color;
    }
}
