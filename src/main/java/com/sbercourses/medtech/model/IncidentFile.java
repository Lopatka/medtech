package com.sbercourses.medtech.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import javax.persistence.*;

@Entity
@Table(name = "incidents_files")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@SequenceGenerator(name = "default_gen", sequenceName = "incidents_files_seq", allocationSize = 1)
public class IncidentFile extends GenericModel {
    @Column(name = "path", nullable = false)
    String path;

    @Column(name = "file_name", nullable = false)
    String fileName;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "incident_id", foreignKey = @ForeignKey(name = "FK_INCIDENT_FILES_INCIDENT"), nullable = false)
    private Incident incident;
}
