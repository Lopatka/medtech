package com.sbercourses.medtech.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;


@Entity
@Table(name = "incidents_details")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@SequenceGenerator(name = "default_gen", sequenceName = "incidents_details_seq", allocationSize = 1)
@IdClass(IncidentDetailPK.class)
public class IncidentDetail {
    @Id
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "incident_id", foreignKey = @ForeignKey(name = "FK_INCIDENT_DETAILS_INCIDENT"))
    private Incident incident;

    @Id
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "detail_id", foreignKey = @ForeignKey(name = "FK_INCIDENT_DETAILS_DETAIL"))
    private Detail detail;

    @Column(name = "count", nullable = false)
    private Long count;
}
