package com.sbercourses.medtech.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


import javax.persistence.*;

@Entity
@Table(name = "manufacturers")
@NoArgsConstructor
@Getter
@Setter
@SequenceGenerator(name = "default_gen", sequenceName = "manufacturers_seq", allocationSize = 1)
public class Manufacturer extends GenericModel {

    @Column(name = "title", nullable = false)
    private String title;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "country_id", foreignKey = @ForeignKey(name = "FK_MANUFACTURER_COUNTRY"), nullable = false)
    private Country country;
}