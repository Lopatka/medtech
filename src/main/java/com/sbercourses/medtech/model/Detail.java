package com.sbercourses.medtech.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "details")
@NoArgsConstructor
@Getter
@Setter
@Builder
@AllArgsConstructor
@SequenceGenerator(name = "default_gen", sequenceName = "details_seq", allocationSize = 1)
public class Detail extends GenericModel {

    @Column(name = "title", nullable = false)
    private String title;

    @Column(columnDefinition = "TEXT", name = "description")
    private String description;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "manufacturer_id", foreignKey = @ForeignKey(name = "FK_DETAIL_MANUFACTURER"), nullable = false)
    private Manufacturer manufacturer;

    @Column(name = "serial_number")
    private String serialNumber;

    @Column(name = "article_number")
    private String articleNumber;

    @Column(name = "price")
    private BigDecimal price;

    @Column(name = "amount", columnDefinition = "INTEGER CHECK (amount >= 0)")
    private Integer amount;

    @Column(name = "delivery_date")
    private LocalDate deliveryDate;

    @JsonIgnore
    @ManyToMany(mappedBy = "details", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
    private List<Incident> incidents;
}
