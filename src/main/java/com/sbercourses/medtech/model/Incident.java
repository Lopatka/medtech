package com.sbercourses.medtech.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sbercourses.medtech.model.enums.Criticality;
import com.sbercourses.medtech.model.enums.IncidentStatus;
import lombok.*;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Entity
@Table(name = "incidents")
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@SequenceGenerator(name = "default_gen", sequenceName = "incidents_seq", allocationSize = 1)
public class Incident extends GenericModel {

    @Column(name = "title", nullable = false)
    private String title;

    @Column(columnDefinition="TEXT", name = "description")
    private String description;

    @Column(columnDefinition="TEXT", name = "repair_description")
    private String repairDescription;

    @Enumerated(EnumType.STRING)
    @Column(name = "criticality", nullable = false)
    private Criticality criticality;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", foreignKey = @ForeignKey(name = "FK_INCIDENT_USER"), nullable = false)
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "repairer_id", foreignKey = @ForeignKey(name = "FK_INCIDENT_REPAIRER"))
    private User repairer;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "device_id", foreignKey = @ForeignKey(name = "FK_INCIDENT_DEPARTMENT"), nullable = false)
    private Device device;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false)
    private IncidentStatus status;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH},
            fetch = FetchType.LAZY)
    @JoinTable(name = "incidents_details",
            joinColumns = @JoinColumn(name = "incident_id"), foreignKey = @ForeignKey(name = "FK_INCIDENTS_DETAILS"),
            inverseJoinColumns = @JoinColumn(name = "detail_id"), inverseForeignKey = @ForeignKey(name = "FK_DETAILS_INCIDENTS"))
    @JsonIgnore
    private List<Detail> details;

    @OneToMany(mappedBy = "incident", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JsonIgnore
    private Set<IncidentFile> incidentFiles;

    @Column(name = "started_working_when")
    private LocalDateTime startedWorkingWhen;

    @Column(name = "completed_when")
    private LocalDateTime completedWhen;

    @Column(name = "closed_when")
    private LocalDateTime closedWhen;
}
