package com.sbercourses.medtech.REST.controller;

import com.sbercourses.medtech.dto.device.DeviceDTO;
import com.sbercourses.medtech.dto.device.DeviceIncidentDTO;
import com.sbercourses.medtech.model.*;
import com.sbercourses.medtech.service.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest/devices")
//CORS Filters
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Tag(name = "Оборудование", description = "Контроллер для работы с оборудованием")
@SecurityRequirement(name = "Bearer Authentication")
public class DeviceController {
    private final DeviceService deviceService;


    public DeviceController(DeviceService deviceService) {
        this.deviceService = deviceService;
    }

    @Operation(description = "Получить список оборудования", method = "list")
    @RequestMapping(value = "/list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Device>> list() {
        return ResponseEntity.status(HttpStatus.OK)
                .body(deviceService.listAll());
    }

    @Operation(description = "Получить информацию об оборудовании по его ID", method = "getOne")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DeviceDTO> getOne(@PathVariable(value = "id") Long id) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(new DeviceDTO(deviceService.getOne(id)));
    }

    @Operation(description = "Получить список инцидентов оборудования по его ID", method = "getDeviceIncidents")
    @RequestMapping(value = "/{id}/incidents", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<DeviceIncidentDTO> getDeviceIncidents(@PathVariable(value = "id") Long id) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(deviceService.getDeviceIncidents(id));
    }
    @Operation(description = "Добавить оборудование", method = "add")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Device> add(@RequestBody DeviceDTO newDevice) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(deviceService.createFromDTO(newDevice));
    }

    @Operation(description = "Обновить информацию оборудования", method = "update")
    @RequestMapping(value = "/{id}/update", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Device> update(@PathVariable(value = "id") Long id, @RequestBody DeviceDTO deviceDTO) {
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(deviceService.updateFromDTO(deviceDTO, id));
    }
    @Operation(description = "Удалить оборудование по Id", method = "delete")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> delete(@RequestParam(value = "deviceId") Long deviceId) {
        deviceService.delete(deviceId);
        return ResponseEntity.status(HttpStatus.OK).body("Оборудование успешно удалено");
    }

}
