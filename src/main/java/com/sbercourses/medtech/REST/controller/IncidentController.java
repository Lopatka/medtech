package com.sbercourses.medtech.REST.controller;


import com.sbercourses.medtech.dto.incident.IncidentDTO;
import com.sbercourses.medtech.dto.incident.IncidentDetailsDTO;
import com.sbercourses.medtech.dto.incident.IncidentRestDTO;
import com.sbercourses.medtech.exception.*;
import com.sbercourses.medtech.model.Incident;
import com.sbercourses.medtech.model.IncidentFile;
import com.sbercourses.medtech.model.enums.IncidentStatus;
import com.sbercourses.medtech.service.IncidentFileService;
import com.sbercourses.medtech.service.IncidentService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@RestController
@RequestMapping("/rest/incidents")
//CORS Filters
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Tag(name = "Заявки", description = "Контроллер для работы с заявками")
@SecurityRequirement(name = "Bearer Authentication")
public class IncidentController {
    private final IncidentService incidentService;
    private final IncidentFileService incidentFileService;

    public IncidentController(IncidentService incidentService, IncidentFileService incidentFileService) {
        this.incidentService = incidentService;
        this.incidentFileService = incidentFileService;
    }

    @Operation(description = "Получить список заявок", method = "list")
    @RequestMapping(value = "/list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<IncidentRestDTO>> list() {
        return ResponseEntity.status(HttpStatus.OK)
                .body(incidentService.listAllDTO());
    }

    @Operation(description = "Получить информацию о заявке по ее ID", method = "getOne")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<IncidentRestDTO> getOne(@PathVariable(value = "id") Long id) throws IncidentGetOneException {
        return ResponseEntity.status(HttpStatus.OK)
                .body(new IncidentRestDTO(incidentService.getOneSecure(id)));
    }

    @Operation(description = "Создать заявку", method = "add")
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<IncidentRestDTO> add(@RequestBody IncidentRestDTO incidentRestDTO) {
        IncidentRestDTO result = new IncidentRestDTO(incidentService.createFromDTO(incidentRestDTO));
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(result);
    }

    @PreAuthorize("hasAuthority('MEDICAL_WORKER')")
    @Operation(description = "Редактировать заявку", method = "update")
    @RequestMapping(value = "/{incidentId}/update", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<IncidentRestDTO> update(@PathVariable Long incidentId,
                                                    @RequestBody IncidentDTO incidentDTO) throws IncidentUpdateException {
        Incident incident = incidentService.updateFromDTOSecure(incidentDTO, incidentId);
        IncidentRestDTO incidentRestDTO = new IncidentRestDTO(incident);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(incidentRestDTO);
    }

    @PreAuthorize("hasAuthority('ENGINEER')")
    @Operation(description = "Изменить детали заявки", method = "updateDetails")
    @RequestMapping(value = "/update-details", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<IncidentRestDTO> updateDetails(@RequestBody IncidentDetailsDTO incidentDetailsDTO) throws IncidentUpdateDetailsException {
        Incident incident = incidentService.updateDetailsFromDTOSecure(incidentDetailsDTO);
        IncidentRestDTO incidentRestDTO = new IncidentRestDTO(incident);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(incidentRestDTO);
    }

    @PreAuthorize("hasAuthority('MEDICAL_WORKER') || hasAuthority('ENGINEER')")
    @Operation(description = "Изменить статус заявки", method = "updateStatus")
    @RequestMapping(value = "/updateStatus/{incidentId}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<IncidentRestDTO> updateStatus(@PathVariable Long incidentId,
                                                        @RequestBody IncidentStatus incidentStatus) throws IncidentUpdateStatusException {
        Incident incident = incidentService.updateStatus(incidentStatus, incidentId);
        IncidentRestDTO incidentRestDTO = new IncidentRestDTO(incidentService.update(incident));
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(incidentRestDTO);
    }

    @Operation(description = "Добавить файлы к заявке", method = "updateFiles")
    @RequestMapping(value = "/{incidentId}/updateFiles", method = RequestMethod.POST, consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<List<IncidentFile>> updateFiles(@PathVariable Long incidentId,
                                                          @RequestParam("files") MultipartFile[] files) throws IncidentCreateFilesException {
        incidentFileService.createFilesSecure(files, incidentId);
        return ResponseEntity.status(HttpStatus.CREATED)
                .body(incidentFileService.listAllById(incidentId));
    }

    @Operation(description = "Удалить файл по fileId и id заявки", method = "deleteFile")
    @RequestMapping(value = "/{incidentId}/delete-file/{fileId}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> deleteFile(@PathVariable Long fileId, @PathVariable Long incidentId) throws IncidentDeleteFileException {
        incidentFileService.deleteFileSecure(fileId, incidentId);
        return ResponseEntity.status(HttpStatus.OK)
                .body("Файл с id: " + fileId + " успешно удален");
    }

    @Operation(description = "Удалить все файлы по id заявки", method = "deleteAllFiles")
    @RequestMapping(value = "/{incidentId}/delete-all-files", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> deleteAllFiles(@PathVariable Long incidentId) throws IncidentDeleteFileException {
        incidentFileService.deleteAllFilesSecure(incidentId);
        return ResponseEntity.status(HttpStatus.OK)
                .body("Все файлы с id заявки: " + incidentId + " успешно удалены");
    }

    @Operation(description = "Скачать файл по id", method = "downloadFile")
    @RequestMapping(value = "/download", method = RequestMethod.GET, produces = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<Resource> downloadFile(@Param(value = "incidentId") Long incidentId,
                                                 @Param(value = "fileId") Long fileId) throws IncidentGetOneException, IOException  {
        IncidentFile incidentFile = incidentFileService.getOneSecure(incidentId, fileId);
        Path path = Paths.get(incidentFile.getPath());
        ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));

        return ResponseEntity.ok()
                .headers(this.headers(path.getFileName().toString()))
                .contentLength(path.toFile().length())
                .contentType(MediaType.parseMediaType("application/octet-stream"))
                .body(resource);
    }

    private HttpHeaders headers(String name) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + name);
        headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
        headers.add("Pragma", "no-cache");
        headers.add("Expires", "0");
        return headers;
    }
}
