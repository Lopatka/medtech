package com.sbercourses.medtech.REST.controller;

import com.sbercourses.medtech.dto.LoginDTO;
import com.sbercourses.medtech.jwtsecurity.JwtTokenUtil;
import com.sbercourses.medtech.service.UserService;
import com.sbercourses.medtech.service.userdetails.CustomUserDetailsService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("/rest/user")
@Tag(name = "Пользователь", description = "Контроллер для работы с пользователями")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserController {
    private final CustomUserDetailsService authenticationService;
    private final JwtTokenUtil jwtTokenUtil;
    private final UserService userService;


    public UserController(CustomUserDetailsService authenticationService,
                          JwtTokenUtil jwtTokenUtil, UserService userService) {
        this.authenticationService = authenticationService;
        this.jwtTokenUtil = jwtTokenUtil;
        this.userService = userService;
    }

    @Operation(description = "Аутентификация", method = "list")
    @RequestMapping(value = "/auth", method = POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<?> auth(@RequestBody LoginDTO loginDTO) {
        HashMap<String, Object> response = new HashMap<>();
        if (!userService.checkPassword(loginDTO)) {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body("Unauthorized user!");
        }
        else {
            UserDetails foundUser = authenticationService.loadUserByUsername(loginDTO.getUsername());
            String token = jwtTokenUtil.generateToken(foundUser);
            response.put("token", token);
            return ResponseEntity.ok().body(response);
        }
    }
}

