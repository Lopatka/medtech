package com.sbercourses.medtech.REST.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sbercourses.medtech.dto.incident.IncidentDetailsDTO;
import com.sbercourses.medtech.dto.incident.IncidentRestDTO;
import com.sbercourses.medtech.jwtsecurity.JwtTokenUtil;
import com.sbercourses.medtech.model.Detail;
import com.sbercourses.medtech.model.Incident;
import com.sbercourses.medtech.model.IncidentDetail;
import com.sbercourses.medtech.model.IncidentFile;
import com.sbercourses.medtech.model.enums.Criticality;
import com.sbercourses.medtech.model.enums.IncidentStatus;
import com.sbercourses.medtech.service.userdetails.CustomUserDetailsService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class IncidentControllerTest {
    @Autowired
    public MockMvc mvc;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    Jackson2ObjectMapperBuilder mapperBuilder;

    @Autowired
    private CustomUserDetailsService customUserDetailsService;

    private String token;

    private static final String ROLE_MEDICAL_WORKER_NAME = "smirnovanton";
    private static final Long ROLE_MEDICAL_WORKER_ID = 11L;
    private static final String ROLE_ENGINEER_NAME = "ivanovIvan";
    private static final Long ROLE_ENGINEER_ID = 8L;
    private static final Long DEVICE_ID = 13L;


    private HttpHeaders generateHeaders(String username) {
        HttpHeaders headers = new HttpHeaders();
        token = generateToken(username);
        headers.add("Authorization", "Bearer " + token);
        return headers;
    }

    private String generateToken(String userName) {
        return jwtTokenUtil.generateToken(customUserDetailsService.loadUserByUsername(userName));
    }

    //TODO: запускается с теста businessLogic остальные тесты по отдельности не работают переделать
    //Чтобы тест запустился нужно закомментировать WebSecurityConfig->SecurityFilterChain
    @Test
    public void businessLogic() throws Exception {
        IncidentRestDTO incidentRestDTOResult = createIncident();
        incidentRestDTOResult = updateStatusIncident(IncidentStatus.NEW, incidentRestDTOResult, ROLE_MEDICAL_WORKER_NAME);
        incidentRestDTOResult = updateStatusIncident(IncidentStatus.WORKING, incidentRestDTOResult, ROLE_ENGINEER_NAME);
        assertEquals(ROLE_ENGINEER_ID, incidentRestDTOResult.getRepairerId());
        incidentRestDTOResult = updateFiles(incidentRestDTOResult, ROLE_ENGINEER_NAME);
        incidentRestDTOResult = updateDetailsIncident(incidentRestDTOResult, ROLE_ENGINEER_NAME);
        //incidentRestDTOResult = deleteFile(incidentRestDTOResult, ROLE_ENGINEER_NAME);
        incidentRestDTOResult = updateStatusIncident(IncidentStatus.COMPLETED, incidentRestDTOResult, ROLE_ENGINEER_NAME);
        incidentRestDTOResult = updateStatusIncident(IncidentStatus.CLOSED, incidentRestDTOResult, ROLE_MEDICAL_WORKER_NAME);
    }

    @Test
    public IncidentRestDTO createIncident() throws Exception {
        IncidentRestDTO incidentRestDTO = new IncidentRestDTO();
        incidentRestDTO.setTitle("Incident for TEST PURPOSE");
        incidentRestDTO.setDescription("Incident Desc for TEST PURPOSE");
        incidentRestDTO.setCriticality(Criticality.MEDIUM);
        incidentRestDTO.setUserId(ROLE_MEDICAL_WORKER_ID);
        incidentRestDTO.setDeviceId(DEVICE_ID);

        String response = mvc.perform(
                        post("/rest/incidents/add")
                                .headers(generateHeaders(ROLE_MEDICAL_WORKER_NAME))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(asJsonString(incidentRestDTO))
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists())
                .andReturn()
                .getResponse()
                .getContentAsString();

        System.out.println(response);
        ObjectMapper objectMapper = new ObjectMapper();
        IncidentRestDTO incidentRestDTOResult = objectMapper.readValue(response, IncidentRestDTO.class);
        System.out.println(incidentRestDTOResult.getId());

        return incidentRestDTOResult;
    }

    @Test
    public IncidentRestDTO updateStatusIncident(IncidentStatus incidentStatus,
                                                IncidentRestDTO incidentRestDTO,
                                                String name) throws Exception {
        Long incidentId = incidentRestDTO.getId();
        String response = mvc.perform(
                        post("/rest/incidents/updateStatus/{incidentId}", incidentId)
                                .headers(generateHeaders(name))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(asJsonString(incidentRestDTO.getStatus()))
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();

        System.out.println(response);
        ObjectMapper objectMapper = mapperBuilder.build();
        IncidentRestDTO incidentRestDTOResult = objectMapper.readValue(response, IncidentRestDTO.class);
        System.out.println(incidentRestDTOResult.getStatus());
        assertEquals(incidentStatus, incidentRestDTOResult.getStatus());
        return incidentRestDTOResult;
    }

    @Test
    public IncidentRestDTO updateFiles(IncidentRestDTO incidentRestDTO,
                                       String name) throws Exception {
        FileInputStream fis1 = new FileInputStream("C:\\Users\\tweru\\Documents\\test1.txt");
        FileInputStream fis2 = new FileInputStream("C:\\Users\\tweru\\Documents\\test2.txt");
        MockMultipartFile A_FILE = new MockMultipartFile("files","test1.txt","multipart/form-data",fis1);
        MockMultipartFile B_FILE = new MockMultipartFile("files","test2.txt","multipart/form-data",fis2);
        Long incidentId = incidentRestDTO.getId();
        String response = mvc.perform(MockMvcRequestBuilders.fileUpload("/rest/incidents/{incidentId}/updateFiles", incidentId)
                        .file(A_FILE)
                        .file(B_FILE)
                        .contentType(MediaType.MULTIPART_FORM_DATA)
                        .headers(generateHeaders(name))
                )
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();

        System.out.println(response);
        ObjectMapper objectMapper = mapperBuilder.build();
        List<IncidentFile> myObjects = Arrays.asList(objectMapper.readValue(response, IncidentFile[].class));

        System.out.println(myObjects);
        return incidentRestDTO;
    }

    @Test
    public IncidentRestDTO updateDetailsIncident(IncidentRestDTO incidentRestDTO,
                                                String name) throws Exception {
        List<IncidentDetail> incidentDetails = new ArrayList<>();
        Detail detailFirst = new Detail();
        Detail detailSecond = new Detail();
        detailFirst.setId(2L);
        detailSecond.setId(3L);
        Incident incident = new Incident();
        incident.setId(incidentRestDTO.getId());
        incidentDetails.add(new IncidentDetail(incident,detailFirst,2L));
        incidentDetails.add(new IncidentDetail(incident,detailSecond,2L));
        IncidentDetailsDTO incidentDetailsDTO = new IncidentDetailsDTO(incidentDetails,"TEST", incidentRestDTO.getId());

        String response = mvc.perform(
                        post("/rest/incidents/update-details")
                                .headers(generateHeaders(name))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(asJsonString(incidentDetailsDTO))
                                .accept(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();

        System.out.println(response);
        ObjectMapper objectMapper = mapperBuilder.build();
        IncidentRestDTO incidentRestDTOResult = objectMapper.readValue(response, IncidentRestDTO.class);
        System.out.println(incidentRestDTOResult.getIncidentDetailsIds());

        return incidentRestDTOResult;
    }

    @Test
    public IncidentRestDTO deleteFile(IncidentRestDTO incidentRestDTO,
                                                 String name) throws Exception {

        String response = mvc.perform(
                        delete("/rest/incidents/{incidentId}/delete-all-files", incidentRestDTO.getId())
                                .headers(generateHeaders(name))
                                .contentType(MediaType.APPLICATION_JSON)
                )
                .andDo(print())
                .andExpect(status().is2xxSuccessful())
                .andReturn()
                .getResponse()
                .getContentAsString();

        System.out.println(response);

        return incidentRestDTO;
    }
    public String asJsonString(Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
